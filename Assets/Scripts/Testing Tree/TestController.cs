﻿using UnityEngine;
using System.Collections.Generic;
using ASTROS.Behaviour.Vehicles;

public class TestController : MonoBehaviour {

    [SerializeField] private List<Transform> itinerary = new List<Transform>();

    [SerializeField] private Transform fatherOfLmus;

    [SerializeField] private List<Transform> allTrees;

    [SerializeField] private bool isStartDriving = false;
    [SerializeField] private bool convoy = false;

    private void Start()
    {
        if (isStartDriving)
        {
            for (int i = 0; i < fatherOfLmus.childCount; i++)
            {
                if (!fatherOfLmus.GetChild(i).gameObject.activeSelf)
                    continue;

                fatherOfLmus.GetChild(i).GetComponent<VehicleBehaviour>().SetItinerary(itinerary, true);
                
                if (i > 0 && convoy)
                    fatherOfLmus.GetChild(i).GetComponent<VehicleBehaviour>().targetToKeepDistanceFrom = fatherOfLmus.GetChild(i - 1).transform;
            }


            for (int i = 0; i < fatherOfLmus.childCount; i++)
            {
                if (!fatherOfLmus.GetChild(i).gameObject.activeSelf)
                    continue;

                fatherOfLmus.GetChild(i).GetComponent<VehicleBehaviour>().isDriving = true;
            }
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            Time.timeScale += 1;

            if (Time.timeScale > 10)
                Time.timeScale = 10;
        }

        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            Time.timeScale -= 1;

            if (Time.timeScale < 1)
                Time.timeScale = 1;
        }
    }

    public void SetItinerary()
    {
        for (int i = 0; i < fatherOfLmus.childCount; i++)
        {
            if (!fatherOfLmus.GetChild(i).gameObject.activeSelf)
                continue;

            fatherOfLmus.GetChild(i).GetComponent<VehicleBehaviour>().SetItinerary(itinerary, true);

            if (i > 0 && convoy)
                fatherOfLmus.GetChild(i).GetComponent<VehicleBehaviour>().targetToKeepDistanceFrom = fatherOfLmus.GetChild(i - 1).transform;
        }


        for (int i = 0; i < fatherOfLmus.childCount; i++)
        {
            if (!fatherOfLmus.GetChild(i).gameObject.activeSelf)
                continue;

            fatherOfLmus.GetChild(i).GetComponent<VehicleBehaviour>().isDriving = true;
        }
    }
}
