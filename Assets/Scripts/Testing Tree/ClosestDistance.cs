﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClosestDistance : MonoBehaviour {

    public float distance;

    public float proj;

    public Vector3 projPoint;

    public Transform[] target;

    private void Update()
    {
        if (target[0] != null && target[1] != null)
            distance = ClosestDistanceFromVector3AndPoint(target[0].position, target[1].position, transform.position);
    }

    private float ClosestDistanceFromVector3AndPoint(Vector3 a, Vector3 b, Vector3 p)
    {
        Vector3 AB = (b - a);
        Vector3 AP = (p - a);

        proj = Vector3.Dot(AB, AP) / AB.sqrMagnitude;

        projPoint = a + Mathf.Clamp(proj, 0f, 1f) * AB;

        return (projPoint - p).magnitude;
    }
}
