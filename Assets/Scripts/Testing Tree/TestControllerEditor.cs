﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(TestController))]
public class TestControllerEditor : Editor {

    TestController testController;

    private void OnEnable()
    {
        testController = (TestController)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Set Itinerary"))
        {
            testController.SetItinerary();
        }
    }
}
