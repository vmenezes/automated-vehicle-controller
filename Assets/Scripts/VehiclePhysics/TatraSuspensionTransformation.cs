﻿using UnityEngine;
using System.Collections;

public class TatraSuspensionTransformation : MonoBehaviour
{
    [SerializeField] private WheelCollider[] m_wheelCollider;
    [SerializeField] private Transform[] m_axisTransform;    
    
    void Update()
    {
        Vector3 colliderPosition;
        Quaternion colliderRotation;

        for (int i = 0; i < m_wheelCollider.Length; i++)
        {
            if (!m_wheelCollider[i].enabled)
                continue;

            m_wheelCollider[i].GetWorldPose(out colliderPosition, out colliderRotation);

            int side = (m_wheelCollider[i].transform.localPosition.x < 0f) ? 1 : -1;

            m_axisTransform[i].LookAt(colliderPosition);
            m_axisTransform[i].Rotate(new Vector3(0, 90 * side, 0));
        }
    }
}
