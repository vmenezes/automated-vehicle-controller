﻿using UnityEngine;
using System.Collections;

public class NormalSuspensionTransformation : MonoBehaviour
{

    [SerializeField] private Transform[] m_axisTransform;
    [SerializeField] private WheelCollider[] m_wheelCollider;

    void Update()
    {
        Vector3 colliderPosition;
        Quaternion colliderRotation;

        for (int i = 0; i < m_wheelCollider.Length; i++)
        {
            if (!m_wheelCollider[i].enabled)
                continue;

            m_wheelCollider[i].GetWorldPose(out colliderPosition, out colliderRotation);

            m_axisTransform[i].transform.position = colliderPosition;
        }
    }
}
