﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using CustomCollision = System.Collections.Generic.KeyValuePair<ObstacleRectangleCollider, ObstacleRectangleCollider>;

public class CollisionCounter : MonoBehaviour {

    private static CollisionCounter _instance = null;
    public static CollisionCounter instance
    {
        get {  return _instance; }
        private set { _instance = value; }
    }

    [SerializeField] private int collisionCounter = 0;

    private List<ObstacleRectangleCollider> colliders = new List<ObstacleRectangleCollider>();

    private List<CustomCollision> collisions = new List<CustomCollision>();
    
    private void Awake()
    {
        instance = this;
    }

    public void collisionDetected()
    {
        collisionCounter++;
    }

    private void Update()
    {
        if (colliders.Count > 1)
        {
            for (int i = 0; i < colliders.Count -1; i++)
                for (int j = i +1; j < colliders.Count; j++)
                    DetectCollision(colliders[i], colliders[j]);
        }
    }

    private void DetectCollision(ObstacleRectangleCollider a, ObstacleRectangleCollider b)
    {
        CustomCollision c = new CustomCollision(a, b);

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                if (IsIntersecting(a.corners[i], a.corners[(i + 1) % 4], b.corners[j], b.corners[(j + 1) % 4]))
                {
                    if (!collisions.Contains(c))
                    {
                        collisions.Add(c);

                        if (a.debugCollisionCounter || b.debugCollisionCounter)
                        {
                            collisionDetected();

                            //ThrowCollisionEnter(a);
                            //ThrowCollisionEnter(b);
                        }
                    }

                    return;
                }
            }
        }

        collisions.Remove(c);
    }

    private void ThrowCollisionEnter(ObstacleRectangleCollider c)
    {
        c.SendMessage("OnCustomCollisionEnter");
    }

    private bool IsIntersecting(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
    {
        float denominator = ((b.x - a.x) * (d.z - c.z)) - ((b.z - a.z) * (d.x - c.x));
        float numerator1 = ((a.z - c.z) * (d.x - c.x)) - ((a.x - c.x) * (d.z - c.z));
        float numerator2 = ((a.z - c.z) * (b.x - a.x)) - ((a.x - c.x) * (b.z - a.z));

        // Detect coincident lines (has a problem, read below)
        if (denominator == 0)
            return numerator1 == 0 && numerator2 == 0;

        float r = numerator1 / denominator;
        float s = numerator2 / denominator;

        return (r >= 0 && r <= 1) && (s >= 0 && s <= 1);
    }

    public void RegisterCollider(ObstacleRectangleCollider collider)
    {
        if (collider != null)
        {
            colliders.Add(collider);
        }
    }

    public void UnregisterCollider(ObstacleRectangleCollider collider)
    {
        colliders.Remove(collider);
    }
}
