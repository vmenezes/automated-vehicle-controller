﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeSpatialHash : MonoBehaviour
{
    private static TreeSpatialHash _instance = null;
    public static TreeSpatialHash instance
    {
        get
        {
            if (_instance != null)
                return _instance;

            GameObject obj = new GameObject("TreeSpatialHash");
            _instance = obj.AddComponent<TreeSpatialHash>();

            return _instance;
        }
        private set
        {
            _instance = value;
        }
    }

    [SerializeField]
    private Vector2 hashSize = new Vector2(100, 200);

    public int[] startIndex, countIndex, treeIndex;


    #region debug variables
    [SerializeField]
    private bool drawDebug = false;
    [SerializeField]
    private bool drawFields = false;
    [SerializeField]
    private int drawInit = 0;
    [SerializeField]
    private int maxDrawCount = 20;
    [SerializeField]
    private int nodeSize = 0;
    [SerializeField]
    private float sphereSize = 10f;
    #endregion

    List<int>[] tempPositions;

    private void Awake()
    {
        instance = this;

        Transform terrain = Terrain.activeTerrain.transform;
        TreeInstance[] trees = Terrain.activeTerrain.terrainData.treeInstances;

        Vector3 terrainSize = Terrain.activeTerrain.terrainData.size;

        //List<int>[] tempPositions = new List<int>[((int)hashSize.x + 1) * ((int)hashSize.x + 1)];
        tempPositions = new List<int>[((int)hashSize.x + 1) * ((int)hashSize.x + 1)];

        for (int i = 0; i < trees.Length; i++)
        {
            int index = getTreeIndex(trees[i].position);

            if (tempPositions[index] == null)
            {
                tempPositions[index] = new List<int>();
            }

            tempPositions[index].Add(i);
        }

        startIndex = new int[(int)((hashSize.x + 1) * (hashSize.x + 1))];
        countIndex = new int[(int)((hashSize.x + 1) * (hashSize.x + 1))];
        treeIndex = new int[trees.Length];

        startIndex[0] = 0;
        for (int i = 0; i < tempPositions.Length; i++)
        {
            if (tempPositions[i] == null)
            {
                countIndex[i] = 0;
                if (i > 0)
                {
                    startIndex[i] = startIndex[i - 1] + countIndex[i - 1];
                }
            }
            else
            {
                if (i > 0)
                {
                    startIndex[i] = startIndex[i - 1] + countIndex[i - 1];
                }

                for (int j = 0; j < tempPositions[i].Count; j++)
                {
                    treeIndex[startIndex[i] + j] = tempPositions[i][j];
                }

                countIndex[i] = tempPositions[i].Count;
            }
        }
    }

    public Vector3[] GetAllTreesInNode(int[] index)
    {
        List<Vector3> trees = new List<Vector3>();

        for (int i = 0; i < 9; i++)
        {
            for (int j = index[2 * i]; j < index[2 * i + 1]; j++)
            {
                trees.Add(GetTreeWorldPosition(j));
            }
        } 

        return trees.ToArray();
    }

    public void GetTreesIndex(Vector3 position, ref int[] allTrees)
    {
        if (startIndex == null || startIndex.Length == 0)
            return;

        if (countIndex == null || countIndex.Length == 0)
            return;

        Transform terrain = Terrain.activeTerrain.transform;
        Vector3 terrainSize = Terrain.activeTerrain.terrainData.size;

        Vector3 localPositionNormalized = position - terrain.position;
        localPositionNormalized.x /= terrainSize.x;
        localPositionNormalized.y /= terrainSize.y;
        localPositionNormalized.z /= terrainSize.z;

        int index = getTreeIndex(localPositionNormalized);

        if (index < 0)
            return;

        if (allTrees == null || allTrees.Length != 18)
            allTrees = new int[18];

        for (int i = 0; i < 18; i++)
            allTrees[i] = -1;        

        bool up = index >= hashSize.x;
        bool left = index % (int)hashSize.x > 0;
        bool right = index % hashSize.x < hashSize.x - 1;
        bool down = index < (hashSize.x - 1) * hashSize.x;

        // First Line
        if (up)
        {
            if (left)
            {
                allTrees[0] = startIndex[index - (int)hashSize.x - 1];
                allTrees[1] = startIndex[index - (int)hashSize.x - 1] + countIndex[index - (int)hashSize.x - 1];
            }

            allTrees[2] = startIndex[index - (int)hashSize.x];
            allTrees[3] = startIndex[index - (int)hashSize.x] + countIndex[index - (int)hashSize.x];

            if (right)
            {
                allTrees[4] = startIndex[index - (int)hashSize.x + 1];
                allTrees[5] = startIndex[index - (int)hashSize.x + 1] + countIndex[index - (int)hashSize.x + 1];
            }
        }

        // Second Line
        {
            if (left)
            {
                allTrees[6] = startIndex[index - 1];
                allTrees[7] = startIndex[index - 1] + countIndex[index - 1];
            }
            
            allTrees[8] = startIndex[index];
            allTrees[9] = startIndex[index] + countIndex[index];

            if (right)
            {
                allTrees[10] = startIndex[index + 1];
                allTrees[11] = startIndex[index + 1] + countIndex[index + 1];
            }
        }

        // Third Line
        if (down)
        {
            if (left)
            {
                allTrees[12] = startIndex[index + (int)hashSize.x - 1];
                allTrees[13] = startIndex[index + (int)hashSize.x - 1] + countIndex[index + (int)hashSize.x - 1];
            }

            allTrees[14] = startIndex[index + (int)hashSize.x];
            allTrees[15] = startIndex[index + (int)hashSize.x] + countIndex[index + (int)hashSize.x];

            if (right)
            {
                allTrees[16] = startIndex[index + (int)hashSize.x + 1];
                allTrees[17] = startIndex[index + (int)hashSize.x + 1] + countIndex[index + (int)hashSize.x + 1];
            }
        }
    }

    private int getTreeIndex(Vector3 treePosition)
    {
        if (treePosition.x > 1 || treePosition.x < 0)
            return -1;

        if (treePosition.z > 1 || treePosition.z < 0)
            return -1;

        float xGap = 1 / hashSize.x;
        float yGap = 1 / hashSize.x;

        return (int)hashSize.x * Mathf.FloorToInt(treePosition.z / yGap) + Mathf.FloorToInt(treePosition.x / xGap);
    }

    void OnDrawGizmos()
    {
        if (drawDebug)
        {
            Gizmos.color = Color.red;

            int count = 0;

            #region Draw the selected trees
            int[] index = new int[18];
            GetTreesIndex(transform.position, ref index);

            Vector3[] trees = GetAllTreesInNode(index);

            if (trees != null)
            {
                nodeSize = trees.Length;
                for (int i = drawInit; i < trees.Length; i++)
                {
                    Gizmos.DrawSphere(trees[i], sphereSize);
                    count++;

                    if (count > maxDrawCount)
                        return;
                }
            }
            #endregion

            #region Draw using the static vector
            //for (int i = drawInit; i < treeIndex.Length; i++)
            //{
            //    Gizmos.DrawSphere(GetTreeWorldPosition(i), sphereSize);
            //    //print(GetTreeWorldPosition(i));
            //    count++;

            //    if (count > maxDrawCount)
            //        return;
            //}
            #endregion

            #region Draw using the dynamic vector
            //if (tempPositions != null)
            //{
            //    for (int i = drawInit; i < tempPositions.Length; i++)
            //    {
            //        if (tempPositions[i] != null)
            //        {
            //            for (int j = 0; j < tempPositions[i].Count; j++)
            //            {
            //                Gizmos.DrawSphere(GetTreeWorldPosition(tempPositions[i][j]), sphereSize);
            //                count++;

            //                if (count > maxDrawCount)
            //                    return;
            //            }
            //        }
            //    }
            //}
            #endregion
        }

        if (drawFields)
        {
            Gizmos.color = Color.blue;

            Transform terrain = Terrain.activeTerrain.transform;
            Vector3 terrainSize = Terrain.activeTerrain.terrainData.size;

            Vector3 cubeSize = new Vector3(terrainSize.x / hashSize.x, 10f, terrainSize.z / hashSize.x);

            for (int i = 0; i < hashSize.x; i++)
            {
                for (int j = 0; j < hashSize.x; j++)
                {
                    Vector3 center = new Vector3(i / hashSize.x * terrainSize.x + cubeSize.x / 2f, 0f, j / hashSize.x * terrainSize.z + cubeSize.z / 2f) + terrain.position;

                    Gizmos.DrawWireCube(center, cubeSize);
                }
            }
        }
    }

    public Vector3 GetTreeWorldPosition(int tree)
    {
        TreeInstance[] trees = Terrain.activeTerrain.terrainData.treeInstances;

        Vector3 position = trees[treeIndex[tree]].position;

        position.x *= Terrain.activeTerrain.terrainData.size.x;
        position.y *= Terrain.activeTerrain.terrainData.size.y;
        position.z *= Terrain.activeTerrain.terrainData.size.z;

        return position + Terrain.activeTerrain.transform.position;
    }

    //private Vector3 GetTreeWorldPosition(int nodeIndex, int treeIndex)
    //{
    //    TreeInstance[] trees = Terrain.activeTerrain.terrainData.treeInstances;

    //    Vector3 position = trees[tempPositions[nodeIndex][treeIndex]].position;

    //    position.x *= Terrain.activeTerrain.terrainData.size.x;
    //    position.y *= Terrain.activeTerrain.terrainData.size.y;
    //    position.z *= Terrain.activeTerrain.terrainData.size.z;

    //    return position + Terrain.activeTerrain.transform.position;
    //}
}
