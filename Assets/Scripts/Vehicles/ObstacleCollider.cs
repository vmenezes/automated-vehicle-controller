﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollider : MonoBehaviour {

    [SerializeField] private Vector3 _position = Vector3.zero;

    public Vector3 localPosition
    {
        get
        {
            return _position;
        }
    }

    public Vector3 position
    {
        get
        {
            return transform.position + transform.rotation * _position;
        }
    }
}
