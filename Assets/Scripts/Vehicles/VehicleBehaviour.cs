﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using ASTROS.Behaviour.Vehicles.Controllers;

namespace ASTROS.Behaviour.Vehicles
{
    /// <summary>
    /// O comportamento do veiculo. Essa classe deve ter um vehicle engine.
    /// Os comportamentos incluem:
    /// - Manter distancia do alvo
    /// - Controle de velocidade
    /// - Estacionar
    /// - Progresso no itinerario
    /// - Seguir caminho
    /// </summary>
    [RequireComponent(typeof(VehicleEngine))]
    public class VehicleBehaviour : MonoBehaviour
    {
        private class VehicleBehaviourObstacleInfo
        {
            private bool _isTree;
            public bool isTree
            {
                get
                {
                    return _isTree;
                }
            }

            private ObstacleCircleCollider _info;
            public ObstacleCircleCollider info
            {
                get
                {
                    return _info;
                }
            }

            public VehicleBehaviourObstacleInfo(ObstacleCircleCollider collider, bool isTree)
            {
                _info = collider;
                _isTree = isTree;
            }
        }

        /// <summary>
        /// E o status retornado para terminar o movimento.
        /// </summary>
		public enum FinishStatus
        {
            ERROR,
            SUCCESS,
            ABORT
        };

        #region Steering Behaviour variables

        [SerializeField]
        private bool useSteeringBehaviour = true;

        /// <summary>
        /// largura do sensor maior de teste de colisao, é uma visao maior para detectar possiveis obstaculos para colisao
        /// </summary>
        [SerializeField]
        private float sensorWidth = 30f;
        /// <summary>
        /// profundidade do sensor maior de teste de colisao, e uma visao maior para detectar possiveis obstaculos para colisao
        /// </summary>
        [SerializeField]
        private float sensorHeight = 30f;
        /// <summary>
        /// largura do cubo de deteccao de obstaculos perigosos. Dentro dos objetos vistos pelo sensor, dangerousObstacles sao aqueles que e necessario um desvio imediato. 
        /// </summary>
		[SerializeField]
        private float dangerousObstacleCubeWidthDetection = 3.5f;
        /// <summary>
        /// distancia que o cubo de obstaculos perigosos enxerga.
        /// </summary>
		[SerializeField]
        private float seeAhead = 7f;
        /// <summary>
        /// forca maxima de evitar o alvo - essa forca aumenta ou diminui medida pela distancia do alvo ate o obstaculo. Quanto mais proximo mais forte e a forca de repulsao.
        /// </summary>
		[SerializeField]
        private float maxEvadeStrength = 200f;
        /// <summary>
        /// Angulo maximo que o agente ira desviar do destino. Caso o desvio faca o agente divirja do destino um angulo maior que maxPathDivergeAngle ele se dirige novamente para o destino, ignorando o steering.
        /// </summary>
		[SerializeField]
        private float maxPathDivergeAngle = 60f;
        /// <summary>
        /// raio de colisao da lista de obstaculos. Como estou fazendo a colisao na mao e a colisao e de uma esfera ao redor do obstaculo, e necessario saber o raio, estou aproximando todas as arvores para o mesmo raio.
        /// </summary>
		[SerializeField]
        private float treeColliderRadius = 2f;
        /// <summary>
        /// tempo para nao recalcular o obstaculo mais perigoso a cada frame. Serve para o veiculo nao 'tremilicar' quando desviar de um obstaculo.
        /// </summary>
		[SerializeField]
        private float keepDangerousObstacleMaxTime = 1f;

        [SerializeField]
        private bool debug = false;

        private int oldTreeIndex;

        private float m_timer = 0f;

        /// <summary>
        /// lista de obstaculos dentro do sensor maior, todos os obstaculos visiveis.
        /// </summary>
        [SerializeField]
        List<VehicleBehaviourObstacleInfo> obstacles = new List<VehicleBehaviourObstacleInfo>();

        /// <summary>
        /// obstaculo mais perigoso. E necessario para os calculos de desvio do obstaculo atual.
        /// </summary>
        [SerializeField]
        private VehicleBehaviourObstacleInfo dangerousObstacle;


        private Transform parentOfTrees = null;
        /// <summary>
        /// todas as arvores que estao sendo testadas com a colisao no sensor, para verificar se ha o perigo de necessidade de desvio.
        /// são duas posições que representam índices dentro do hash espacial das árvores (TreeSpatialHash)
        /// </summary>
        public int[] allTrees;

        private TreeSpatialHash treeHash;

        /// <summary>
        /// Todas as árvores do terreno são guardadas aqui.
        /// </summary>
        private TreeInstance[] trees;
        #endregion

        /// <summary>
        /// Motor do veiculo. Todas as funcionalidades que involvem fisica sao implementadas no motor.
        /// Ex.: Caso queira acelerar, utilize as funcoes implementadas do motor.
        /// </summary>
        private VehicleEngine m_vehicleEngine; // Motor do veiculo
        private Action<FinishStatus> m_OnArriveCallbackDelegate;

        /// <summary>
        /// velocidade maxima do veiculo. Pode ser modificada para cada diferente veiculo.
        /// </summary>
		[SerializeField]
        private float _maxVelocity = 30f;
        public float maxVelocity
        {
            set
            {
                _maxVelocity = value;
            }
            get
            {
                return _maxVelocity;
            }
        } // Get/Set maximum velocity
        private float initialMaxVelocity; // Velocidade máxima inicial

        /// <summary>
        /// Lista de waypoints do itinerario que o veiculo deve percorrer.
        /// </summary>
		[SerializeField]
        private List<Transform> targetList = new List<Transform>(); // Lista de waypoints do caminho
        /// <summary>
        /// O veiculo deve manter uma distancia saudavel desse transform
        /// </summary>
		[SerializeField]
        private Transform _targetToKeepDistanceFrom;

        /// <summary>
        /// get/set targetToKeepDistanceFrom
        /// O veiculo deve manter uma distancia saudavel desse transform
        /// </summary>
		public Transform targetToKeepDistanceFrom
        {
            set
            {
                _targetToKeepDistanceFrom = value;

                if (targetToKeepDistanceFrom != null)
                {
                    targetVehicleBehaviour = targetToKeepDistanceFrom.GetComponent<VehicleBehaviour>();
                } else
                {
                    targetVehicleBehaviour = null;
                }
            }
            get
            {
                return _targetToKeepDistanceFrom;
            }
        }

        /// <summary>
        /// Referencia da posicao frontal do veiculo
        /// </summary>
		private Transform _m_Front = null; // A frente do veiculo. E necessario pois cada veiculo tem dimensoes diferentes.
        public Transform m_Front
        {
            set
            {
                _m_Front = value;
            }
            get
            {
                if (_m_Front == null)
                    m_Front = transform.FindChild("Front");

                return _m_Front;
            }
        } // Get/Set frente do veiculo
        /// <summary>
        /// Referencia da posicao traseira do veiculo
        /// </summary>
		private Transform _m_Back = null; // Traseira do veiculo. E necessario pois cada veiculo tem dimensoes diferentes.
        public Transform m_Back
        {
            set
            {
                _m_Back = value;
            }
            get
            {
                if (_m_Back == null)
                    m_Back = transform.FindChild("Back");

                return _m_Back;
            }
        } // Get/Set traseira do veiculo
        /// <summary>
        /// Referencia da posicao central do veiculo
        /// </summary>
        private Transform _m_Center = null; // Centro do veiculo. E necessario pois cada veiculo tem dimensoes diferentes.
        public Transform m_Center
        {
            set
            {
                _m_Center = value;
            }
            get
            {
                if (_m_Center == null)
                {
                    _m_Center = new GameObject("Center").transform;
                    _m_Center.parent = transform;
                    _m_Center.position = (m_Front.position + m_Back.position) / 2f;
                }

                return _m_Center;
            }
        } // Get/Set centro do veiculo

        /// <summary>
        /// Bool que diz se o veiculo esta dirigindo em um itinerario. O veiculo pode estar dirigindo mesmo estando parado, quando ele esta parado atras do alvo que ele deve seguir no comboio, por exemplo
        /// </summary>
        [SerializeField]
        private bool _isDriving = false;
        public bool isDriving
        {
            set
            {
                if (targetList.Count < 2)
                    return;

                _isDriving = value;

                if (targetIndex < 0)
                    targetIndex = GetClosestWayPoint();
            }
            get
            {
                return _isDriving;
            }
        } // Get/Set isDriving

        /// <summary>
        /// Vehicle behaviour de targetToKeepDistanceFrom . E o vehicleBehaviour do alvo que eu tenho que seguir.
        /// E necessario para a comunicacao entre as viaturas.
        /// </summary>
		private VehicleBehaviour targetVehicleBehaviour;

        /// <summary>
        /// Progresso sobre o caminho. E uma porcentagem relacionada ao progresso do veiculo num certo caminho(que e um subcaminho do itinerario).
        /// </summary>
		public float m_ItineraryProgress = 0f;
        public float ItineraryProgress
        {
            private set
            {
                m_ItineraryProgress = (m_ItineraryLength - value);
            }
            get
            {
                return m_ItineraryProgress / m_ItineraryLength;
            }
        } // Get/Set progresso
        /// <summary>
        /// Tamanho do caminho. E uma porcentagem relacionada ao progresso do veiculo num certo caminho(que e um subcaminho do itinerario).
        /// </summary>
		public float m_ItineraryLength;
        private float ItineraryLength
        {
            set
            {
                m_ItineraryLength = value;
                m_ItineraryProgress = 0f;
                if (targetList.Count > 0)
                {
                    m_ItineraryProgress = -(Vector3.Distance(transform.position, targetList[0].position));
                } else
                {
                    Debug.LogError("ERRO: targetList not set!!!");
                }
            }
            get
            {
                return m_ItineraryLength;
            }
        } // Get/Set tamanho do caminho

        /// <summary>
        /// Indice sobre a lista de waypoints(targetList) 
        /// </summary>
		[SerializeField]
        private int targetIndex = -1;

        /// <summary>
        /// Encontra a frente e a traseira do veículo. Deve acontecer antes do start porque suas variaveis sao utilizadas em outras funcoes Start.
        /// </summary>
        private void Awake()
        {
            targetIndex = -1;
        }

        /// <summary>
        /// Encontra e inicia variaveis da classe.
        /// </summary>
        private void Start()
        {
            allTrees = new int[2];

            m_vehicleEngine = GetComponent<VehicleEngine>();

            trees = Terrain.activeTerrain.terrainData.treeInstances;

            treeHash = TreeSpatialHash.instance;

            initialMaxVelocity = maxVelocity;

            if (targetToKeepDistanceFrom != null)
                targetVehicleBehaviour = targetToKeepDistanceFrom.GetComponent<VehicleBehaviour>();
        }

        /// <summary>
        /// Chamadas da fisica do veiculo.
        /// </summary>
        private void FixedUpdate()
        {
            CarControl();

            if (targetIndex < 0 || targetIndex > targetList.Count - 1)
                return;

            SpeedControl();

            if (useSteeringBehaviour)
            {
                Sensor();

                GetObstacleOnPath();
            }

            // testa se o numero de pontos no itinerario sao validos, e necessario no minimo dois pontos - um inicial e um final
            if (targetList.Count > 1 && targetIndex < targetList.Count - 1)
                if (ReachedWaypoint(targetList[targetIndex - 1].position, targetList[targetIndex].position))
                    targetIndex++;

            ItineraryProgressUpdate();
        }

        //private void Update()
        //{
        //    try
        //    {
        //        if (targetIndex < 0 || targetIndex > targetList.Count - 1)
        //            return;

        //        SpeedControl();

        //        if (useSteeringBehaviour)
        //        {
        //            Sensor();

        //            GetObstacleOnPath();
        //        }

        //        // testa se o numero de pontos no itinerario sao validos, e necessario no minimo dois pontos - um inicial e um final
        //        if (targetList.Count > 1 && targetIndex < targetList.Count - 1)
        //            if (ReachedWaypoint(targetList[targetIndex - 1].position, targetList[targetIndex].position))
        //                targetIndex++;

        //        ItineraryProgressUpdate();
        //    } catch (Exception e)
        //    {
        //        Debug.Log("VehicleBehaviour: Exception caught! Finishing safely...");
        //        OnError(e.ToString());
        //        throw e;
        //    }
        //}

        ///// <summary>
        ///// Chamadas não fisicas do veiculo.
        ///// </summary>
        //private void Update()
        //{
        //	try
        //	{
        //		if (targetIndex < 0 || targetIndex > targetList.Count - 1)
        //			return;

        //		CheckForListConsistency();

        //		SpeedControl();

        //		if (allTrees.Count > 0)
        //		{
        //			Sensor();

        //			DirCorrection();
        //		}

        //		if (targetList.Count > 1 && targetIndex < targetList.Count - 1)
        //			if (ReachedWaypoint(targetList[targetIndex - 1].position, targetList[targetIndex].position))
        //				targetIndex++;

        //		ItineraryProgressUpdate();
        //	}
        //	catch (Exception e)
        //	{
        //		Debug.Log("VehicleBehaviour: Exception caught! Finishing safely...");
        //		OnError(e.ToString());
        //		throw e;
        //	}
        //}

        ////////////////////////////////////////////
        private void OnError(string err)
        {
            if (m_OnErrorCallbacks != null)
                m_OnErrorCallbacks(err);
            FinalizeItinerary(FinishStatus.ERROR);
        }
        private Action<string> m_OnErrorCallbacks;
        public void AddOnErrorCallback(Action<string> cb)
        {
            if (m_OnErrorCallbacks == null)
                m_OnErrorCallbacks = cb;
            m_OnErrorCallbacks += cb;
        }
        public void RemoveOnErrorCallback(Action<string> cb)
        {
            if (m_OnErrorCallbacks == null)
                return;
            m_OnErrorCallbacks -= cb;
        }
        ////////////////////////////////////////////

        /// <summary>
        /// Atualiza o progresso sobre o caminho.
        /// </summary>
        private void ItineraryProgressUpdate()
        {
            if (targetList.Count > 0 && targetIndex < targetList.Count - 1 && targetList[targetIndex] != null)
            {
                ItineraryProgress = Mathf.Clamp(Vector3.Distance(transform.position, targetList[targetIndex].position), 0, ItineraryLength);
            }
        }

        /// <summary>
        /// Limpa a lista de waypoints caso seu conteúdo foi deletado.
        /// </summary>
        private void CheckForListConsistency()
        {
            if (targetList.Count > 0)
            {
                for (int i = 0; i < targetList.Count; i++)
                    if (targetList[i] == null)
                    {
                        //targetList.Clear();
                        //keepDistanceFromTarget = null;
                        //targetIndex = -1;
                        targetList.Clear();
                        targetToKeepDistanceFrom = null;
                        FinalizeItinerary(FinishStatus.SUCCESS);
                        break;
                    }
            }
        }

        /// <summary>
        /// Controle geral do carro.
        /// </summary>
        private void CarControl()
        {
            // caso nao tenha itinerario, para
            if (targetList.Count == 0)
            {
                m_vehicleEngine.Brake();
                return;
            }
            // caso nao esteja dirigindo, para
            if (!isDriving)
            {
                m_vehicleEngine.Brake();
                return;
            }
            // caso o veiculo que devemos estar atras esteja na frente, para
            if (!MyKeepDistanceFromTargetIsAhead())
            {
                m_vehicleEngine.Brake();
                return;
            }


            LookAtTarget();

            // verifica se e par estacionar ou continuar dirigindo - verifica o ultimo ponto e o alvo que esta a seguir
            bool isToStop = (targetIndex >= targetList.Count - 1) || (targetToKeepDistanceFrom != null && !targetVehicleBehaviour.isDriving);

            if (isToStop)
            {
                StopAtTarget();
            } else
            {
                Drive();
            }
        }

        /// <summary>
        /// Distance check. Verifies the closest distance from the current position of the transform
        /// and the line segment.
        /// </summary>
        /// <param name="startPoint">The line segment's start point</param>
        /// <param name="endPoint">The line segment's end point</param>
        /// <returns>True if it reached the end point, false if its not</returns>
        private bool ReachedWaypoint(Vector3 startPoint, Vector3 endPoint)
        {
            if (!isDriving)
                return false;

            Vector3 point = m_Front.position;

            startPoint.y = endPoint.y = point.y;

            Vector3 line = endPoint - startPoint;
            Vector3 pointFromLine = point - startPoint;

            float dot = Vector3.Dot(line, pointFromLine);

            // in case the line segment has length equals to 0
            if (line.sqrMagnitude == 0)
                return true;

            float valueOverLine = dot / line.sqrMagnitude;

            //return (valueOverLine > 1) || (endPoint - point).sqrMagnitude < 0.64f;

            //if ((valueOverLine > 0.95f) || (endPoint - point).sqrMagnitude < 0.64f)
            if ((valueOverLine > (1f - (6f / line.magnitude))) || (endPoint - point).sqrMagnitude < 0.64f)
                return true;

            return false;
        }

        /// <summary>
        /// Verifica se o veiculo deve acelerar o desacelerar.
        /// </summary>
        private void Drive()
        {
            if (m_vehicleEngine.velocity < maxVelocity)
            {
                m_vehicleEngine.Accelerate(true);
            } else
            {
                m_vehicleEngine.EngineBrake();
            }
        }

        /// <summary>
        /// Comportamento de estacionar.
        /// 
        /// Verifica a posicao sob o itinerario e o status do veiculo que deve seguir, caso exista.
        /// 
        /// Faz testes de distancia da menor distancia do segmento de reta e distancia do veiculo a ser seguido.
        /// </summary>
        private void StopAtTarget()
        {
            if (targetIndex > targetList.Count - 1)
            {
                FinalizeItinerary(FinishStatus.SUCCESS);
                return;
            }

            bool finalize = false;

            if (targetToKeepDistanceFrom != null && !targetVehicleBehaviour.isDriving)
            {
                float distanceFromKeepDistanceFromTarget = DistanceFromTarget(targetVehicleBehaviour.m_Back);
                finalize |= distanceFromKeepDistanceFromTarget < 5f;
            } else
            {
                bool isUnderDistanceThreshold = ReachedWaypoint(targetList[targetIndex - 1].position, targetList[targetIndex].position);
                finalize |= isUnderDistanceThreshold;
            }

            float distanceFromLastWayPoint = DistanceFromTarget(targetList[targetList.Count - 1]);
            finalize |= distanceFromLastWayPoint < 2f;


            //isUnderDistanceThreshold = distance < initialMaxVelocity;

            if (finalize)
            {
                FinalizeItinerary(FinishStatus.SUCCESS);
            } else
            {
                Drive();
            }
        }

        /// <summary>
        /// Comportamento quando nao consegue atingir o alvo. Isso acontece quando, por exemplo, o veiculo esta muito rapido e nao consegue fazer a curva.
        /// </summary>
        private IEnumerator Maneuver()
        {
            int numberOfTries = 0;
            float distance = 0f;

            while (numberOfTries < 1)
            {
                distance = DistanceFromTarget(targetList[targetIndex]);

                bool isUnderDistanceThreshold = distance < 5f;

                if (isUnderDistanceThreshold)
                {
                    numberOfTries++;

                    while (isUnderDistanceThreshold)
                    {
                        distance = DistanceFromTarget(targetList[targetIndex]);
                        isUnderDistanceThreshold = distance < 3f;

                        if (!isDriving)
                            yield break;

                        yield return null;
                    }
                }

                if (!isDriving)
                    yield break;

                yield return null;
            }

            distance = 0f;
            while (distance < 10f)
            {
                distance = DistanceFromTarget(targetList[targetIndex]);
                m_vehicleEngine.RotateToTarget(m_Front.position + DirCorrection());
                yield return null;
            }
        }

        /// <summary>
        /// Atualiza a velocidade maxima baseada em, por exemplo, o veiculo da frente, terreno, etc.
        /// </summary>
        private void SpeedControl()
        {
            if (targetToKeepDistanceFrom == null)
            {
                maxVelocity = initialMaxVelocity;
                return;
            }

            if (targetVehicleBehaviour == null || !targetVehicleBehaviour.transform.Equals(targetToKeepDistanceFrom))
                targetVehicleBehaviour = targetToKeepDistanceFrom.GetComponent<VehicleBehaviour>();

            float distance = DistanceFromTarget(targetVehicleBehaviour.m_Back);
            bool isUnderDistanceThreshold = distance < 10f; // threshold distance between vehicles.

            if (isUnderDistanceThreshold)
            {
                if (!targetVehicleBehaviour.isDriving)
                {
                    maxVelocity = 10f; // If the vehicle is arriving at a certain point the velocity should decreased otherwise the vehicle will not stop at the correct point. 
                } else
                {
                    maxVelocity = targetVehicleBehaviour.m_vehicleEngine.velocity - 5f;
                }
            } else
            {
                maxVelocity = targetVehicleBehaviour.initialMaxVelocity + 1f;
            }
        }

        /// <summary>
        /// Verifica todos os obstaculos dentro da lista e retorna uma forca na direcao do desvio.
        /// 
        /// Essa força e proporcional a velocidade e a distancia do obstaculo.
        /// </summary>
        /// <returns>Uma forca de desvio</returns>
        private Vector3 DirCorrection()
        {
            if (dangerousObstacle == null)
                return Vector3.zero;

            Vector3 tempObstaclePos = dangerousObstacle.info.position;
            tempObstaclePos.y = m_Front.position.y;

            Vector3 dirOfObstacle = tempObstaclePos - m_Front.position;
            dirOfObstacle.y = 0f;
            dirOfObstacle = dirOfObstacle.normalized;

            Vector3 myForward = m_Front.forward;
            myForward.y = 0f;
            myForward = myForward.normalized;

            Vector3 dirOfWaypoint = targetList[targetIndex].position - m_Front.position;
            dirOfWaypoint.y = 0f;
            dirOfWaypoint = dirOfWaypoint.normalized;

            Vector3 evadeDir;

            if (Vector3.Angle(myForward, dirOfObstacle) < 10f * ((tempObstaclePos - m_Center.position).sqrMagnitude < 16f + dangerousObstacle.info.radius ? 9 : 1))
            {
                evadeDir = Vector3.Cross(Vector3.up, dirOfObstacle);
            } else
            {
                evadeDir = (m_Front.position + myForward * seeAhead * 2f - tempObstaclePos).normalized;
            }

            int reverseDir = ((Vector3.Angle(myForward, dirOfWaypoint) > maxPathDivergeAngle && dangerousObstacle.isTree) ? CheckForBestWayAroundObstacle(evadeDir, myForward, tempObstaclePos) : 1);

            // draw the evade direction from obstacle
            if (debug)
                Debug.DrawRay(dangerousObstacle.info.position, evadeDir * 100f * reverseDir, Color.red);

            return evadeDir * reverseDir * EvadeStrength();
        }

        /// <summary>
        /// Verifica a melhor direcao para contornar o obstaculo.
        /// 
        /// As vezes um lado esta bloqueado e e necessario utilizar o outro. Aqui e calculado o melhor lado.
        /// </summary>
        /// <param name="evasionDir">direcao de evasao</param>
        /// <param name="dirOfObstacle">direcao do obstaculo</param>
        /// <returns></returns>
        private int CheckForBestWayAroundObstacle(Vector3 evadeDir, Vector3 myForward, Vector3 tempObstaclePos)
        {
            Vector3 tempTargetPos = targetList[targetIndex].position;
            tempTargetPos.y = m_Front.position.y;

            Vector3 dirOfTargetFromObstacle = (tempTargetPos - tempObstaclePos).normalized;

            float angle = Vector3.Angle(dirOfTargetFromObstacle, evadeDir);

            if (angle > 85f)
                return -1;

            return 1;
        }

        /// <summary>
        /// Calcula a forca de desvio de alvo. Leva em consideração a distância do alvo para o cálculo.
        /// 
        /// Quanto mais perto mais forte é a forca.
        /// </summary>
        /// <returns>A forca de desvio.</returns>
        private float EvadeStrength()
        {
            if (dangerousObstacle == null)
                return 0;

            float distanceFromObstacle = (m_Front.position - dangerousObstacle.info.position).magnitude - dangerousObstacle.info.radius;

            return maxEvadeStrength - distanceFromObstacle / 30f * maxEvadeStrength;
        }

        /// <summary>
        /// Pega o obstaculo mais proximo no caminho direto, em linha reta do alvo.
        /// 
        /// O calculo se da pela iteracao entre todos os possiveis obstaculos representados pela variavel obstacles
        /// verificando a interseccao com o colisor de teste posicionado logo a frente do veiculo e verificando o mais proximo.
        /// </summary>
        /// <returns>A transform do obstaculo em linha reta mais proximo</returns>
        private void GetObstacleOnPath()
        {
            if (m_timer > keepDangerousObstacleMaxTime / Time.timeScale)
            {
                VehicleBehaviourObstacleInfo obstacle = null;

                Vector3 myForward = m_Front.forward;
                myForward.y = 0f;
                myForward = myForward.normalized;

                Vector3 rectangleCenter = m_Front.position + myForward * seeAhead / 2f;
                Vector3 rectangleExtents = new Vector3(dangerousObstacleCubeWidthDetection / 2f, 3f, seeAhead / 2f);

                for (int i = 0; i < obstacles.Count; i++)
                {
                    //float colliderRadius = obstacles[i].GetComponent<SphereCollider>().radius * obstacles[i].lossyScale.x;

                    if (IntersectsWithRectangle(rectangleCenter, rectangleExtents, obstacles[i].info.position, obstacles[i].info.radius))
                    {
                        if (obstacle == null)
                        {
                            obstacle = obstacles[i];
                        } else
                        {
                            if (IsCloserToMyTransform(obstacles[i].info.position, obstacle.info.position))
                            {
                                obstacle = obstacles[i];
                            }
                        }
                    }
                }

                dangerousObstacle = obstacle;
                m_timer = 0f;
            }

            m_timer += Time.deltaTime;
        }

        /// <summary>
        /// Retorna qual das posicoes esta mais proxima do veiculo
        /// </summary>
        /// <param name="v1">posicao v1</param>
        /// <param name="v2">posicao v2</param>
        /// <returns>verdadeiro caso v1 seja mais proximo que v2 do veiculo</returns>
        private bool IsCloserToMyTransform(Vector3 v1, Vector3 v2)
        {
            return (v1 - transform.position).sqrMagnitude < (v2 - transform.position).sqrMagnitude;
        }

        /// <summary>
        /// Verifica se o objeto intersecta com o cubo/plano passado por parametro.
        /// 
        /// Essa funcao e utilizada na deteccao de objetos logo a frente do veiculo, para obstaculos que necessitam
        /// de desvio imediato.
        /// </summary>
        /// <param name="center">centro do cubo/plano</param>
        /// <param name="size">tamanho do cubo</param>
        /// <param name="target">posicao do objeto de comparacao</param>
        /// <returns>verdadeiro caso colide, false caso nao colida</returns>
        private bool IntersectsWithRectangle(Vector3 center, Vector3 extents, Vector3 target, float colliderRadius = 1)
        {
            Vector3 myForward = m_Front.forward;
            myForward.y = 0f;
            myForward = myForward.normalized;

            Vector3 myRight = m_Front.right;
            myRight.y = 0f;
            myRight = myRight.normalized;

            Vector3 targetPosition = target;
            targetPosition.y = 0f;

            center.y = 0f;

            #region using plane circunference intersection (dont detect circle inside)

            Vector3 pos1, pos2, pos3, pos4;
            pos1 = center + myForward * extents.z - myRight * extents.x;
            pos2 = center + myForward * extents.z + myRight * extents.x;
            pos3 = center - myForward * extents.z + myRight * extents.x;
            pos4 = center - myForward * extents.z - myRight * extents.x;

            float distance = ClosestDistanceFromVector3AndPoint(pos1, pos2, targetPosition);

            if (distance < colliderRadius)
                return true;

            distance = ClosestDistanceFromVector3AndPoint(pos2, pos3, targetPosition);

            if (distance < colliderRadius)
                return true;

            distance = ClosestDistanceFromVector3AndPoint(pos3, pos4, targetPosition);

            if (distance < colliderRadius)
                return true;

            distance = ClosestDistanceFromVector3AndPoint(pos4, pos1, targetPosition);

            if (distance < colliderRadius)
                return true;

            if (IsAtRight(pos2 - pos1, targetPosition - pos1) == 1 &&
                IsAtRight(pos3 - pos2, targetPosition - pos2) == 1 &&
                IsAtRight(pos4 - pos3, targetPosition - pos3) == 1 &&
                IsAtRight(pos1 - pos4, targetPosition - pos4) == 1)
            {
                return true;
            }

            #endregion

            #region using unitys colliders

            //Collider[] colliders = Physics.OverlapBox(center, extents, Quaternion.LookRotation(myForward));

            //foreach (Collider c in colliders)
            //    if (c.gameObject == target)
            //        return true;
            #endregion

            #region teste de colisao entre um rectangle nao rotado e ponto (pode ser esfera)

            //if (Mathf.Abs(center.x - targetPosition.x) < extents.x + colliderRadius)
            //{
            //    if (Mathf.Abs(center.z - targetPosition.z) < extents.z + colliderRadius)
            //    {
            //        return true;
            //    }
            //}

            #endregion

            return false;
        }

        /// <summary>
        /// Calcula a menor distância entre um ponto e um segmento de reta
        /// </summary>
        /// <param name="a">ponto inicial do segmento de reta</param>
        /// <param name="b">ponto final do segmento de reta</param>
        /// <returns>a distancia entre um segmento de reta e um ponto</returns>
        private float ClosestDistanceFromVector3AndPoint(Vector3 a, Vector3 b, Vector3 p)
        {
            Vector3 AB = (b - a);
            Vector3 AP = (p - a);

            float proj = Vector3.Dot(AB, AP) / AB.sqrMagnitude;

            Vector3 projPoint = a + Mathf.Clamp(proj, 0f, 1f) * AB;

            return (projPoint - p).magnitude;
        }

        /// <summary>
        /// Verifica se o vetor v2 esta a direita ou a esquerda de v1.
        /// 
        /// E necessaria para verificar a melhor direcao para se tomar em relacao ao ponto destino.
        /// Caso o destino esteja a esquerda, e melhor virar para a esquerda.
        /// 
        /// E retornado um inteiro para simplesmente inverter caso seja a esquerda.
        /// </summary>
        /// <param name="v1">vetor 1</param>
        /// <param name="v2">vetor 2</param>
        /// <returns>1 caso esteja a direita e -1 caso esteja a esquerda.</returns>
        private int IsAtRight(Vector3 v1, Vector3 v2)
        {
            if (v1.sqrMagnitude > 1)
                v1 = v1.normalized;

            if (v2.sqrMagnitude > 1)
                v2 = v2.normalized;

            if (v1.z * v2.x > v1.x * v2.z)
                return 1;
            else
                return -1;
        }

        /// <summary>
        /// Rota a roda para atingir o alvo.
        /// </summary>
        private void LookAtTarget()
        {
            if (targetIndex >= targetList.Count)
                return;

            Vector3 finalPosition;

            if (dangerousObstacle == null)
                finalPosition = SeekPositionProjectedOnItinerary();
            //finalPosition = targetList[targetIndex].position;
            else
                finalPosition = dangerousObstacle.info.position + DirCorrection();

            m_vehicleEngine.RotateToTarget(finalPosition);

            // draw the movement direction vector
            if (debug)
                Debug.DrawLine(m_Front.position, finalPosition, Color.magenta);
        }

        //public float aheadSeekDistance = 2f;

        /// <summary>
        /// Posição na qual o veículo irá olhar dentro de um segmento do itinerário.
        /// 
        /// É utilizado quando o veículo, ao realizar uma curva, sai um pouco da linha traçada pelo path finding.
        /// Aqui é tratado para o veículo retornar ao itinerário.
        /// 
        /// Utiliza uma projeção sobre o itinerário e o veículo da um seek para uma posição um pouco à sua frente.
        /// </summary>
        /// <returns>A posição projetada no itinerário que o veículo deve seguir</returns>
        private Vector3 SeekPositionProjectedOnItinerary()
        {
            Vector3 startSegmentPoint = targetList[targetIndex - 1].position;
            Vector3 endSegmentPoint = targetList[targetIndex].position;

            Vector3 point = m_Front.position - startSegmentPoint;

            startSegmentPoint.y = endSegmentPoint.y = point.y = m_Front.position.y;

            float aheadSeekDistance = 2f;

            Vector3 segment = (endSegmentPoint - startSegmentPoint);

            // Projection of my position over the itinerary segment.
            Vector3 projPointOverSegment = (Vector3.Dot(segment, point) / segment.sqrMagnitude) * segment + startSegmentPoint;

            if (debug)
                Debug.DrawLine(m_Front.position, projPointOverSegment, Color.yellow);

            Vector3 segmentDirection = segment.normalized;

            // Position that the vehicle will seek to
            Vector3 seekPosition = projPointOverSegment + segmentDirection * aheadSeekDistance;

            Vector3 directionOfEndSegmentPointFromProjPointOverSegment = (endSegmentPoint - projPointOverSegment).normalized;

            // Verifies if the seekPosition is inside the segment testing the directions of each other
            if (segmentDirection == directionOfEndSegmentPointFromProjPointOverSegment)
            {
                return seekPosition;
            } else
            {
                // Here it checks if its the last position in the itinerary so it can extend the projection to the next segment
                if (targetIndex >= targetList.Count - 1)
                    return seekPosition - segment * 2 * aheadSeekDistance;

                float newAheadSeekDistance = aheadSeekDistance - (seekPosition - endSegmentPoint).magnitude;

                startSegmentPoint = endSegmentPoint;
                endSegmentPoint = targetList[targetIndex + 1].position;

                segmentDirection = (endSegmentPoint - startSegmentPoint).normalized;

                seekPosition = startSegmentPoint + segmentDirection * newAheadSeekDistance;

                return seekPosition;
            }
        }

        /// <summary>
        /// Testa se é para o veículo que eu tenho que seguir esta a minha frente ou não. Usado para testar se tu deve esperar o veaculo ou não.
        /// </summary>
        /// <returns>Retorna TRUE se eu estou na frente e FALSE se estou atras.</returns>
        private bool MyKeepDistanceFromTargetIsAhead()
        {
            if (DesagregateFromConvoy())
            {
                targetToKeepDistanceFrom = null;
            }

            // faz a comparacao se esta ou nao a frente comparando indices sob o itinerario
            if (targetToKeepDistanceFrom != null)
            {
                if (targetIndex - 1 == targetVehicleBehaviour.targetIndex)
                {
                    if (ItineraryProgress >= targetVehicleBehaviour.ItineraryProgress)
                    {
                        return false;
                    }
                } else if (targetVehicleBehaviour.isDriving && targetIndex > targetVehicleBehaviour.targetIndex)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Testa se e para desagregar do comboio.
        /// 
        /// Verifica se os itinerarios sao iguais nos dois lados, comparando o targetIndex de cada um em sua lista de itinerario. Caso nao sejam, desagrega.
        /// </summary>
        /// <returns></returns>
        private bool DesagregateFromConvoy()
        {
            return targetToKeepDistanceFrom != null &&
                    ((targetIndex < targetVehicleBehaviour.targetList.Count && targetList[targetIndex] != targetVehicleBehaviour.targetList[targetIndex]) ||
                    (targetVehicleBehaviour.isDriving && targetVehicleBehaviour.targetIndex < targetList.Count && targetList[targetVehicleBehaviour.targetIndex] != targetVehicleBehaviour.targetList[targetVehicleBehaviour.targetIndex]));
        }

        /// <summary>
        /// Calcula a distancia entre eu e other;
        /// </summary>
        /// <param name="other">O outro transform.</param>
        /// <returns>A distancia entre eu e other.</returns>
        public float DistanceFromTarget(Transform other)
        {
            if (m_Front == null || other == null)
                return 0.0f;

            Vector3 m_position = m_Front.position;
            Vector3 targetPosition = other.position;

            m_position.y = targetPosition.y = 0f;

            return Vector3.Distance(m_position, targetPosition);
        }

        /// <summary>
        /// Seta uma lista de alvos para o veículo seguir, sem manter uma distancia minima.
        /// </summary>
        /// <param name="targetList">Lista de alvos</param>
        public void SetItinerary(List<Transform> targetList, bool createInitialPosition)
        {
            Intialize(targetList, createInitialPosition);

            targetToKeepDistanceFrom = null;
        }

        /// <summary>
        /// Set the list of targets wich the vehicle will go throw and the target wich it will keep distance enought not to collide.
        /// </summary>
        /// <param name="targetList">List of targets</param>
        /// <param name="keepDistanceFromTarget">Target to keep distance from</param>
        public void SetItinerary(List<Transform> targetList, Transform keepDistanceFromTarget, bool createInitialPosition)
        {
            Intialize(targetList, createInitialPosition);

            this.targetToKeepDistanceFrom = keepDistanceFromTarget;
        }

        /// <summary>
        /// Aborta uma movimentacao
        /// </summary>
		public void Abort()
        {
            FinalizeItinerary(FinishStatus.ABORT);
        }

        /// <summary>
        /// Inicializa uma movimentacao.
        /// </summary>
        /// <param name="targetList">Lista de pontos do itinerario</param>
        /// <param name="createInitialPosition">Se ha a necessidade de criar o ponto inicial do veiculo dentro do itinerario</param>
		private void Intialize(List<Transform> targetList, bool createInitialPosition)
        {
            if ((targetList.Count < 2 && !createInitialPosition) || (targetList.Count < 1 && createInitialPosition))
                return;

            targetIndex = -1;

            this.targetList = targetList;

            if (createInitialPosition)
            {
                GameObject currentPos = new GameObject(GetType().ToString() + "InitialPosition");

                currentPos.transform.position = m_Front.position;

                this.targetList.Insert(0, currentPos.transform);
            }

            ItineraryLength = DistanceFromTarget(targetList[1]);
        }

        /// <summary>
        /// Finaliza a movimentacao ajustando as variaveis para seus valores default de estacionado.
        /// </summary>
        /// <param name="finishStatus"></param>
		public void FinalizeItinerary(FinishStatus finishStatus)
        {
            isDriving = false;

            targetIndex = -1;

            if (m_OnArriveCallbackDelegate != null)
                m_OnArriveCallbackDelegate(finishStatus);
        }

        /// <summary>
        /// Calcula o waypoint inicial do veiculo.
        /// 
        /// E necessario pois o itinerario novo cria pontos adicinais para a posicao inicial da viatura.
        /// Entao, caso bote 3 viaturas, e criado 3 pontos adicionais e enviado para as 3, assim a viatura 1 comeca no
        /// 3 ponto e a viatura 2 no segundo e a viatura 3 no primeiro, por exemplo.
        /// </summary>
        /// <returns>O indice do waypoint inicial do veiculo.</returns>
        private int GetClosestWayPoint()
        {
            int waypoint = 0;

            for (int i = 0; i < targetList.Count; i++)
            {
                if (DistanceFromTarget(targetList[i]) < 1)
                {
                    waypoint = i;
                }
            }

            return waypoint + 1;
        }

        /// <summary>
        /// Testes de colisao com os obstaculos listados nas listas.
        /// 
        /// E feito testes de colisao em duas camadas, uma maior, para obstaculos possiveis e outra menor para obstaculos imediatos.
        /// 
        /// A colisao e feita atravez de um teste de colisao retangulo-circunferencia no plano xz.
        /// </summary>
        private void Sensor()
        {
            Vector3 myForward = m_Front.forward;
            myForward.y = 0f;
            myForward = myForward.normalized;

            Vector3 rectangleCenter = m_Front.position + myForward * sensorHeight / 2f;
            Vector3 rectangleExtents = new Vector3(sensorWidth / 2f, 5f, sensorHeight / 2f);

            if (treeHash != null)
            {
                treeHash.GetTreesIndex(transform.position, ref allTrees);

                TreeSensor(rectangleCenter, rectangleExtents, treeColliderRadius);
            }

            ComponentSensor(rectangleCenter, rectangleExtents);
        }

        private void ComponentSensor(Vector3 rectangleCenter, Vector3 rectangleExtents)
        {
            List<ObstacleCollider> objects = CollisionManager.instance.colliders;

            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].transform == transform || targetToKeepDistanceFrom == objects[i].transform)
                    continue;

                ObstacleCircleCollider info = objects[i].GetComponent<ObstacleCircleCollider>();
                if (info != null && GetObjectInObstacles(info) == null)
                {
                    obstacles.Add(new VehicleBehaviourObstacleInfo(info, false));
                }
            }
        }

        private void TreeSensor(Vector3 rectangleCenter, Vector3 rectangleExtents, float treeRadius)
        {
            if (allTrees == null)
                return;

            for (int i = 0; i < 9; i++)
            {
                for (int j = allTrees[2 * i]; j < allTrees[2 * i + 1]; j++)
                {
                    Vector3 position = treeHash.GetTreeWorldPosition(j);

                    if (IntersectsWithRectangle(rectangleCenter, rectangleExtents, position, treeRadius))
                    {
                        VehicleBehaviourObstacleInfo treeInfo = GetTreeInObstacles(position);
                        if (treeInfo == null)
                        {
                            if (parentOfTrees == null)
                            {
                                parentOfTrees = new GameObject("parentOfTrees").transform;
                            }

                            Transform t = new GameObject("TreeHelper").transform;
                            t.parent = parentOfTrees;
                            t.position = position;
                            ObstacleCircleCollider info = t.gameObject.AddComponent<ObstacleCircleCollider>();

                            info.radius = treeRadius;

                            obstacles.Add(new VehicleBehaviourObstacleInfo(info, true));
                        }
                    } else
                    {
                        VehicleBehaviourObstacleInfo treeInfo = GetTreeInObstacles(position);
                        if (treeInfo != null && treeInfo.isTree)
                        {
                            obstacles.Remove(treeInfo);

                            if (treeInfo.isTree)
                            {
                                if (dangerousObstacle == treeInfo)
                                    dangerousObstacle = null;

                                Destroy(treeInfo.info.gameObject);

                                if (parentOfTrees.childCount == 0)
                                    Destroy(parentOfTrees.gameObject);
                            }
                        }
                    }
                }
            }
        }

        private VehicleBehaviourObstacleInfo GetTreeInObstacles(Vector3 tree)
        {
            for (int i = 0; i < obstacles.Count; i++)
            {
                if (!obstacles[i].isTree)
                    continue;

                if ((obstacles[i].info.position - tree).sqrMagnitude < 0.25f)
                    return obstacles[i];
            }

            return null;
        }

        private VehicleBehaviourObstacleInfo GetObjectInObstacles(ObstacleCircleCollider info)
        {
            for (int i = 0; i < obstacles.Count; i++)
            {
                if (obstacles[i].isTree)
                    continue;

                if (obstacles[i].info == info)
                    return obstacles[i];
            }

            return null;
        }

        public void AddOnArriveCallback(Action<FinishStatus> cb)
        {
            if (m_OnArriveCallbackDelegate == null)
            {
                m_OnArriveCallbackDelegate = new Action<FinishStatus>(cb);
                return;
            }

            m_OnArriveCallbackDelegate += cb;
        }

        /// <summary>
        /// Desenha os sensores
        /// </summary>
        private void OnDrawGizmos()
        {
            if (!debug)
                return;

            if (m_Front != null)
            {
                Vector3 myForward = m_Front.forward;
                myForward.y = 0f;
                myForward = myForward.normalized;

                Vector3 myRight = m_Front.right;
                myRight.y = 0f;
                myRight = myRight.normalized;

                #region dangerous obstacle sensor
                {
                    Vector3 rectangleCenter = m_Front.position + myForward * seeAhead / 2f;
                    Vector3 rectangleExtents = new Vector3(dangerousObstacleCubeWidthDetection / 2f, 3f, seeAhead / 2f);

                    // dangerous obstacle rectangle
                    Vector3 pos1, pos2, pos3, pos4;
                    pos1 = rectangleCenter + myForward * rectangleExtents.z - myRight * rectangleExtents.x;
                    pos2 = rectangleCenter + myForward * rectangleExtents.z + myRight * rectangleExtents.x;
                    pos3 = rectangleCenter - myForward * rectangleExtents.z + myRight * rectangleExtents.x;
                    pos4 = rectangleCenter - myForward * rectangleExtents.z - myRight * rectangleExtents.x;

                    Gizmos.color = Color.blue;

                    Gizmos.DrawLine(pos1, pos2);
                    Gizmos.DrawLine(pos2, pos3);
                    Gizmos.DrawLine(pos3, pos4);
                    Gizmos.DrawLine(pos4, pos1);
                }
                #endregion

                #region Sensor
                {
                    Vector3 rectangleCenter = m_Front.position + myForward * sensorHeight / 2f;
                    Vector3 rectangleExtents = new Vector3(sensorWidth / 2f, 3f, sensorHeight / 2f);

                    // dangerous obstacle rectangle
                    Vector3 pos1, pos2, pos3, pos4;
                    pos1 = rectangleCenter + myForward * rectangleExtents.z - myRight * rectangleExtents.x;
                    pos2 = rectangleCenter + myForward * rectangleExtents.z + myRight * rectangleExtents.x;
                    pos3 = rectangleCenter - myForward * rectangleExtents.z + myRight * rectangleExtents.x;
                    pos4 = rectangleCenter - myForward * rectangleExtents.z - myRight * rectangleExtents.x;

                    Gizmos.color = Color.green;

                    Gizmos.DrawLine(pos1, pos2);
                    Gizmos.DrawLine(pos2, pos3);
                    Gizmos.DrawLine(pos3, pos4);
                    Gizmos.DrawLine(pos4, pos1);
                }
                #endregion
            }
        }

        /// <summary>
        /// Desenha a lista de obstáculos
        /// </summary>
        private void OnDrawGizmosSelected()
        {
            if (!debug)
                return;

            for (int i = 0; i < targetList.Count; i++)
            {
                if (targetList[i] == null)
                    continue;

                Gizmos.color = Color.Lerp(Color.red, Color.green, /*targetListReverseOrder ? (targetList.Count - 1 - i) / (float)targetList.Count : */i / (float)targetList.Count);
                Gizmos.DrawSphere(targetList[i].position, 10);

                if (i < targetList.Count - 1 && targetList[i + 1] != null)
                    Gizmos.DrawLine(targetList[i].position, targetList[i + 1].position);
            }
        }
    }
}