﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCircleCollider : ObstacleCollider {

    
	[SerializeField] private float _radius;

    public bool isCounter = false;

    public float radius
    {
        set { _radius = value; }
        get { return _radius; }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawWireSphere(position, radius);
    }
}
