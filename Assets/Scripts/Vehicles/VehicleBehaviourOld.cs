﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using ASTROS.Behaviour.Vehicles.Controllers;

/// <summary>
/// O comportamento do veiculo. Essa classe deve ter um vehicle engine.
/// Os comportamentos incluem:
/// - Manter distancia do alvo
/// - Controle de velocidade
/// - Estacionar
/// - Progresso no itinerario
/// - Seguir caminho
/// </summary>
[RequireComponent(typeof(VehicleEngine))]
public class VehicleBehaviourOld : MonoBehaviour {
    
    private class VehicleBehaviourObstacleInfo
    {
        private bool _isTree;
        public bool isTree
        {
            get { return _isTree; }
        }
        
        private ObstacleCircleCollider _info;
        public ObstacleCircleCollider info
        {
            get { return _info; }
        }

        public VehicleBehaviourObstacleInfo(ObstacleCircleCollider collider, bool isTree)
        {
            _info = collider;
            _isTree = isTree;
        }
    }
    
	public enum FinishStatus
	{
		ERROR,
		MOVE
	}; // Finish Status
       // TODO Acrescentar melhor o comentario do finish status. Provavelmente utilizada pela move vehicles task

    #region Steering Behaviour variables
    [SerializeField] private float sensorWidth = 30f;
    [SerializeField] private float sensorHeight = 30f;
    [SerializeField] private float dangerousObstacleCubeWidthDetection = 3.5f;
    [SerializeField] private float seeAhead = 7f;
    [SerializeField] private float maxEvadeStrength = 200f;
    [SerializeField] private float maxPathDivergeAngle = 60f;
    [SerializeField] private float keepDangerousObstacleMaxTime = 1f;
    [SerializeField] private float treeColliderRadius = 5f;

    private float m_timer = 0f;

    [SerializeField] List<VehicleBehaviourObstacleInfo> obstacles = new List<VehicleBehaviourObstacleInfo>();

    [SerializeField] private VehicleBehaviourObstacleInfo dangerousObstacle;

    private Transform parentOfTrees = null;
    public Vector3[] allTrees;

    private TreeSpatialHash treeHash;
    #endregion

    private VehicleEngine m_vehicleEngine; // Motor do veiculo
	private Action<FinishStatus> m_OnArriveCallbackDelegate;

    [SerializeField] private float _maxVelocity = 30f; // Esse e um valor padrao, deve ser modificado para cada veiculo
    public float maxVelocity
    {
        set { _maxVelocity = value; }
        get { return _maxVelocity; }
    } // Get/Set maximum velocity
    private float initialMaxVelocity; // Velocidade máxima inicial

    [SerializeField] private List<Transform> targetList = new List<Transform>(); // Lista de waypoints do caminho
    [SerializeField] private Transform _keepDistanceFromTarget; // O veiculo deve manter uma distancia saudavel desse transform

    public Transform keepDistanceFromTarget
    {
        set
        {
            _keepDistanceFromTarget = value;

            if (keepDistanceFromTarget != null)
            {
                targetVehicleBehaviour = keepDistanceFromTarget.GetComponent<VehicleBehaviourOld>();
            } else
            {
                targetVehicleBehaviour = null;
            }
        }
        get { return _keepDistanceFromTarget; }
    } // Get/Set do KeepDistanceFromTarget
    private Transform _m_Front; // A frente do veiculo. E necessario pois cada veiculo tem dimensoes diferentes.
    public Transform m_Front
    {
        set { _m_Front = value; }
        get { return _m_Front; }
    } // Get/Set frente do veiculo
    private Transform _m_Back; // Traseira do veiculo. E necessario pois cada veiculo tem dimensoes diferentes.
    public Transform m_Back
    {
        set { _m_Back = value; }
        get { return _m_Back; }
    } // Get/Set traseira do veiculo

    [SerializeField] private bool _isDriving = false; // Bool que diz se o veiculo esta dirigindo em um itinerario. O veiculo pode estar dirigindo mesmo estando parado, quando ele esta parado atras do alvo que ele deve seguir no comboio, por exemplo
    public bool isDriving
    {
        set
        {
            _isDriving = value;

            if (targetIndex < 0)
                targetIndex = GetClosestWayPoint();
        }
        get { return _isDriving; }
    } // Get/Set isDriving

    private VehicleBehaviourOld targetVehicleBehaviour; // Vehicle behaviour de keepDistanceFromTarget . E o vehicleBehaviour do alvo que eu tenho que seguir.

    public float m_ItineraryProgress = 0f; // Progresso sobre o caminho. E uma porcentagem relacionada ao progresso do veiculo num certo caminho(que e um subcaminho do itinerario).
    public float ItineraryProgress
    {
        private set { m_ItineraryProgress = (m_ItineraryLength - value); }
        get { return m_ItineraryProgress / m_ItineraryLength; }
    } // Get/Set progresso
    public float m_ItineraryLength; // Tamanho do caminho. E uma porcentagem relacionada ao progresso do veiculo num certo caminho(que e um subcaminho do itinerario).
    private float ItineraryLength
    {
        set {
                m_ItineraryLength = value;
                m_ItineraryProgress = 0f;
                if (targetList.Count > 0)
                {
                    m_ItineraryProgress = -(Vector3.Distance(transform.position, targetList[0].position));
                } else
                {
                    Debug.LogError("ERRO: targetList not set!!!");
                }
            }
        get { return m_ItineraryLength; }
    } // Get/Set tamanho do caminho

    [SerializeField] private int targetIndex = -1; // Indice sobre a lista de waypoints(targetList) 
    
    /// <summary>
    /// Encontra a frente e a traseira do veículo. Deve acontecer antes do start porque suas variaveis sao utilizadas em outras funcoes Start.
    /// </summary>
    private void Awake()
    {
        m_Front = transform.FindChild("Front");
        m_Back = transform.FindChild("Back");

        targetIndex = -1;
    }

    /// <summary>
    /// Encontra e inicia variaveis da classe.
    /// </summary>
    private void Start()
    {
        m_vehicleEngine = GetComponent<VehicleEngine>();

        treeHash = GameObject.FindGameObjectWithTag("TreeHash").GetComponent<TreeSpatialHash>();

        initialMaxVelocity = maxVelocity;

        if (keepDistanceFromTarget != null)
            targetVehicleBehaviour = keepDistanceFromTarget.GetComponent<VehicleBehaviourOld>();
    }
    
    /// <summary>
    /// Chamadas da fisica do veiculo.
    /// </summary>
    private void FixedUpdate()
    {
		CarControl();        
    }

    /// <summary>
    /// Chamadas não fisicas do veiculo.
    /// </summary>
    private void Update()
    {
        if (targetIndex < 0 || targetIndex > targetList.Count - 1)
            return;
        
        SpeedControl();

        Sensor();

        DirCorrection();

        if (targetList.Count > 1)
            ReachedWaypoint(targetList[targetIndex -1].position, targetList[targetIndex].position);

        ItineraryProgressUpdate();
    }

    /// <summary>
    /// Atualiza o progresso sobre o caminho.
    /// </summary>
    private void ItineraryProgressUpdate()
    {
        if (targetList.Count > 0 && targetIndex < targetList.Count -1 && targetList[targetIndex] != null)
        {
            ItineraryProgress = Mathf.Clamp(Vector3.Distance(transform.position, targetList[targetIndex].position), 0, ItineraryLength);
        }
    }

    /// <summary>
    /// Controle geral do carro.
    /// </summary>
    private void CarControl()
    {
		if (targetList.Count == 0)
		{
			m_vehicleEngine.Brake();
			return;
		}
		if (!isDriving)
		{
			m_vehicleEngine.Brake();
			return;
		}
		if (!myKeepDistanceFromTargetIsAhead())
        {
            m_vehicleEngine.Brake();
            return;
        }
       

        LookAtTarget();        

        bool isToStop = (targetIndex >= targetList.Count -1) || (keepDistanceFromTarget != null && !targetVehicleBehaviour.isDriving);

        if (isToStop)
        {
            StopAtTarget();
        } else
        {
            Drive();
        }
    }

    /// <summary>
    /// Distance check. Verifies the closest distance from the current position of the transform
    /// and the line segment.
    /// </summary>
    /// <param name="startPoint">The line segment's start point</param>
    /// <param name="endPoint">The line segment's end point</param>
    /// <returns>True if it reached the end point, false if its not</returns>
    private void ReachedWaypoint(Vector3 startPoint, Vector3 endPoint)
    {
        Vector3 point = transform.position;

        startPoint.y = endPoint.y = point.y;

        Vector3 line = endPoint - startPoint;
        Vector3 pointFromLine = point - startPoint;

        float dot = Vector3.Dot(line, pointFromLine);

        // in case the line segment has length equals to 0
        if (line.sqrMagnitude == 0)
            targetIndex++;

        float valueOverLine = dot / line.sqrMagnitude;

        //return (valueOverLine > 1) || (endPoint - point).sqrMagnitude < 0.64f;

        if ((valueOverLine > 1) || (endPoint - point).sqrMagnitude < 0.64f)
            targetIndex++;
    }
    
    /// <summary>
    /// Verifica se o veiculo deve acelerar o desacelerar.
    /// </summary>
    private void Drive()
    {
        if (m_vehicleEngine.velocity < maxVelocity)
        {
            m_vehicleEngine.Accelerate(true);
        } else
        {
            m_vehicleEngine.Brake();
        }
    }

    /// <summary>
    /// Comportamento de estacionar.
    /// </summary>
    private void StopAtTarget()
    {
        if (targetIndex > targetList.Count -1)
        {
            isDriving = false;
            return;
        }

        float distance = 0f;

        if (keepDistanceFromTarget == null)
        {
            distance = DistanceFromTarget(targetList[targetIndex]);
        } else
		{
            float distanceFromItinerary = DistanceFromTarget(targetList[targetList.Count -1]);
            float distanceFromKeepDistanceFromTarget = DistanceFromTarget(targetVehicleBehaviour.m_Back);

            distance = (distanceFromItinerary < distanceFromKeepDistanceFromTarget) ? distanceFromItinerary : distanceFromKeepDistanceFromTarget;
        }

		bool isUnderDistanceThreshold = distance < initialMaxVelocity;

        if (isUnderDistanceThreshold)
        {
            bool isUnderSmallestDistanceThreshold = false;

            if (keepDistanceFromTarget == null)
                isUnderSmallestDistanceThreshold = distance < 2f;
            else
                isUnderSmallestDistanceThreshold = distance < 5f;

            if (isUnderSmallestDistanceThreshold)
            {
                m_vehicleEngine.Brake();

                if (keepDistanceFromTarget != null && !targetVehicleBehaviour.isDriving)
                {
                    isDriving = false;
                } else
                {
                    isDriving = false;
                }

				if(m_OnArriveCallbackDelegate != null)
					m_OnArriveCallbackDelegate(FinishStatus.MOVE);
            } else
            {
                //StartCoroutine(Maneuver());

                Drive();
            }            
        } else
        {
            Drive();
        }
    }

    /// <summary>
    /// Comportamento quando nao consegue atingir o alvo. Isso acontece quando, por exemplo, o veiculo esta muito rapido e nao consegue fazer a curva.
    /// </summary>
    private IEnumerator Maneuver()
    {
        int numberOfTries = 0;
        float distance = 0f;

        while (numberOfTries < 1)
        {
            distance = DistanceFromTarget(targetList[targetIndex]);

            bool isUnderDistanceThreshold = distance < 5f;
            
            if (isUnderDistanceThreshold)
            {
                numberOfTries++;

                while (isUnderDistanceThreshold)
                {
                    distance = DistanceFromTarget(targetList[targetIndex]);
                    isUnderDistanceThreshold = distance < 3f;

                    if (!isDriving)
                        yield break;

                    yield return null;
                }
            }

            if (!isDriving)
                yield break;

            yield return null;
        }

        distance = 0f;
        while (distance < 10f)
        {
            distance = DistanceFromTarget(targetList[targetIndex]);
            m_vehicleEngine.RotateToTarget(m_Front.position + DirCorrection());
            yield return null;
        }
    }

    /// <summary>
    /// Atualiza a velocidade maxima baseada em, por exemplo, o veiculo da frente, terreno, etc.
    /// </summary>
    private void SpeedControl()
    {
        if (keepDistanceFromTarget == null)
        {
            maxVelocity = initialMaxVelocity;
            return;
        }

        if (targetVehicleBehaviour == null || !targetVehicleBehaviour.transform.Equals(keepDistanceFromTarget))
            targetVehicleBehaviour = keepDistanceFromTarget.GetComponent<VehicleBehaviourOld>();

        float distance = DistanceFromTarget(targetVehicleBehaviour.m_Back);
        bool isUnderDistanceThreshold = distance < 10f; // threshold distance between vehicles.
        
        if (isUnderDistanceThreshold)
        {
            if (!targetVehicleBehaviour.isDriving)
            {
                maxVelocity = 10f; // If the vehicle is arriving at a certain point the velocity should decreased otherwise the vehicle will not stop at the correct point. 
            } else
            {
                maxVelocity = targetVehicleBehaviour.m_vehicleEngine.velocity - 5f;
            }
        } else
        {
            maxVelocity = targetVehicleBehaviour.initialMaxVelocity + 1f;
        }
    }

    /// <summary>
    /// Verifica todos os obstaculos dentro da lista e retorna uma forca na direcao do desvio.
    /// 
    /// Essa força e proporcional a velocidade e a distancia do obstaculo.
    /// </summary>
    /// <returns>Uma forca de desvio</returns>
    private Vector3 DirCorrection()
    {
        if (m_timer > keepDangerousObstacleMaxTime / Time.timeScale)
        {
            dangerousObstacle = GetObstacleOnPath();
            m_timer = 0f;
        }

        m_timer += Time.deltaTime;

        if (dangerousObstacle == null)
            return Vector3.zero;
                
        Vector3 tempObstaclePos = dangerousObstacle.info.position;
        tempObstaclePos.y = m_Front.position.y;
        
        Vector3 dirOfObstacle = tempObstaclePos - m_Front.position;
        dirOfObstacle.y = 0f;
        dirOfObstacle = dirOfObstacle.normalized;

        Vector3 myForward = m_Front.forward;
        myForward.y = 0f;
        myForward = myForward.normalized;

        Vector3 dirOfWaypoint = targetList[targetIndex].position - m_Front.position;
        dirOfWaypoint.y = 0f;
        dirOfWaypoint = dirOfWaypoint.normalized;

        Vector3 evadeDir;

        if (Vector3.Angle(myForward, dirOfObstacle) < 10f * ((tempObstaclePos - m_Front.position).sqrMagnitude < 16f + dangerousObstacle.info.radius ? 9 : 1))
        {
            evadeDir = Vector3.Cross(Vector3.up, dirOfObstacle);
        } else
        {
            evadeDir = (m_Front.position + myForward * seeAhead * 2f - tempObstaclePos).normalized;
        }
        
        int reverseDir = ((Vector3.Angle(myForward, dirOfWaypoint) > maxPathDivergeAngle && dangerousObstacle.isTree) ? CheckForBestWayAroundObstacle(evadeDir, myForward, tempObstaclePos) : 1);

        // draw the evade direction from obstacle
        Debug.DrawRay(dangerousObstacle.info.position, evadeDir * 100f * reverseDir, Color.red);

        return evadeDir * reverseDir * EvadeStrength();
    }
    
    /// <summary>
    /// Verifica a melhor direcao para contornar o obstaculo.
    /// 
    /// As vezes um lado esta bloqueado e e necessario utilizar o outro. Aqui e calculado o melhor lado.
    /// </summary>
    /// <param name="evasionDir">direcao de evasao</param>
    /// <param name="dirOfObstacle">direcao do obstaculo</param>
    /// <returns></returns>
    private int CheckForBestWayAroundObstacle(Vector3 evadeDir, Vector3 myForward, Vector3 tempObstaclePos)
    {
        Vector3 tempTargetPos = targetList[targetIndex].position;
        tempTargetPos.y = m_Front.position.y;

        Vector3 dirOfTargetFromObstacle = (tempTargetPos - tempObstaclePos).normalized;

        float angle = Vector3.Angle(dirOfTargetFromObstacle, evadeDir);

        if (angle > 85f)
            return -1;
        
        return 1;
    }

    /// <summary>
    /// Calcula a forca de desvio de alvo. Leva em consideração a distância do alvo para o cálculo.
    /// 
    /// Quanto mais perto mais forte é a forca.
    /// </summary>
    /// <returns>A forca de desvio.</returns>
    private float EvadeStrength()
    {
        if (dangerousObstacle == null)
            return 0;
        
        float distanceFromObstacle = (m_Front.position - dangerousObstacle.info.position).magnitude - dangerousObstacle.info.radius;

        return maxEvadeStrength - distanceFromObstacle / 30f * maxEvadeStrength;
    }

    /// <summary>
    /// Pega o obstaculo mais proximo no caminho direto, em linha reta do alvo.
    /// 
    /// O calculo se da pela iteracao entre todos os possiveis obstaculos representados pela variavel obstacles
    /// verificando a interseccao com o colisor de teste posicionado logo a frente do veiculo e verificando o mais proximo.
    /// </summary>
    /// <returns>A transform do obstaculo em linha reta mais proximo</returns>
    private VehicleBehaviourObstacleInfo GetObstacleOnPath()
    {
        VehicleBehaviourObstacleInfo obstacle = null;

        Vector3 myForward = m_Front.forward;
        myForward.y = 0f;
        myForward = myForward.normalized;

        Vector3 rectangleCenter = m_Front.position + myForward * seeAhead / 2f;
        Vector3 rectangleExtents = new Vector3(dangerousObstacleCubeWidthDetection / 2f, 3f, seeAhead / 2f);

        for (int i = 0; i < obstacles.Count; i++)
        {
            //float colliderRadius = obstacles[i].GetComponent<SphereCollider>().radius * obstacles[i].lossyScale.x;

            if (IntersectsWithRectangle(rectangleCenter, rectangleExtents, obstacles[i].info.position, obstacles[i].info.radius))
            {
                if (obstacle == null)
                {
                    obstacle = obstacles[i];
                } else
                {
                    if (IsCloserToMyTransform(obstacles[i].info.position, obstacle.info.position))
                    {
                        obstacle = obstacles[i];
                    }
                }
            }
        }       

        return obstacle;
    }

    /// <summary>
    /// Retorna verdadeiro caso v1 seja mais proximo do veiculo que v2.
    /// </summary>
    /// <param name="v1">posicao v1</param>
    /// <param name="v2">posicao v2</param>
    /// <returns>verdadeiro caso v1 seja mais proximo que v2 do veiculo</returns>
    private bool IsCloserToMyTransform(Vector3 v1, Vector3 v2)
    {
        return (v1 - transform.position).sqrMagnitude < (v2 - transform.position).sqrMagnitude;
    }

    /// <summary>
    /// Verifica se o objeto intersecta com o cubo/plano passado por parametro.
    /// 
    /// Essa funcao e utilizada na deteccao de objetos logo a frente do veiculo, para obstaculos que necessitam
    /// de desvio imediato.
    /// </summary>
    /// <param name="center">centro do cubo/plano</param>
    /// <param name="size">tamanho do cubo</param>
    /// <param name="target">posicao do objeto de comparacao</param>
    /// <returns>verdadeiro caso colide, false caso nao colida</returns>
    private bool IntersectsWithRectangle(Vector3 center, Vector3 extents, Vector3 target, float colliderRadius = 1)
    {
        Vector3 myForward = m_Front.forward;
        myForward.y = 0f;
        myForward = myForward.normalized;

        Vector3 myRight = m_Front.right;
        myRight.y = 0f;
        myRight = myRight.normalized;

        Vector3 targetPosition = target;
        targetPosition.y = 0f;

        center.y = 0f;

        #region using plane circunference intersection (dont detect circle inside)

        Vector3 pos1, pos2, pos3, pos4;
        pos1 = center + myForward * extents.z - myRight * extents.x;
        pos2 = center + myForward * extents.z + myRight * extents.x;
        pos3 = center - myForward * extents.z + myRight * extents.x;
        pos4 = center - myForward * extents.z - myRight * extents.x;

        float distance = ClosestDistanceFromVector3AndPoint(pos1, pos2, targetPosition);

        if (distance < colliderRadius)
            return true;

        distance = ClosestDistanceFromVector3AndPoint(pos2, pos3, targetPosition);

        if (distance < colliderRadius)
            return true;

        distance = ClosestDistanceFromVector3AndPoint(pos3, pos4, targetPosition);

        if (distance < colliderRadius)
            return true;

        distance = ClosestDistanceFromVector3AndPoint(pos4, pos1, targetPosition);

        if (distance < colliderRadius)
            return true;

        if (IsAtRight(pos2 - pos1, targetPosition - pos1) == 1 &&
            IsAtRight(pos3 - pos2, targetPosition - pos2) == 1 &&
            IsAtRight(pos4 - pos3, targetPosition - pos3) == 1 &&
            IsAtRight(pos1 - pos4, targetPosition - pos4) == 1)
        {
            return true;
        }

        #endregion

        #region using unitys colliders

        //Collider[] colliders = Physics.OverlapBox(center, extents, Quaternion.LookRotation(myForward));

        //foreach (Collider c in colliders)
        //    if (c.gameObject == target)
        //        return true;
        #endregion

        #region teste de colisao entre um rectangle nao rotado e ponto (pode ser esfera)

        //if (Mathf.Abs(center.x - targetPosition.x) < extents.x + colliderRadius)
        //{
        //    if (Mathf.Abs(center.z - targetPosition.z) < extents.z + colliderRadius)
        //    {
        //        return true;
        //    }
        //}

        #endregion

        return false;
    }

    /// <summary>
    /// Calcula a menor distância entre um ponto e um segmento de reta
    /// </summary>
    /// <param name="a">ponto inicial do segmento de reta</param>
    /// <param name="b">ponto final do segmento de reta</param>
    /// <returns>a distancia entre um segmento de reta e um ponto</returns>
    private float ClosestDistanceFromVector3AndPoint(Vector3 a, Vector3 b, Vector3 p)
    {
        Vector3 AB = (b - a);
        Vector3 AP = (p - a);

        float proj = Vector3.Dot(AB, AP) / AB.sqrMagnitude;

        Vector3 projPoint = a + Mathf.Clamp(proj, 0f, 1f) * AB;

        return (projPoint - p).magnitude;
    }

    /// <summary>
    /// Verifica se o vetor v2 esta a direita ou a esquerda de v1.
    /// 
    /// E necessaria para verificar a melhor direcao para se tomar em relacao ao ponto destino.
    /// Caso o destino esteja a esquerda, e melhor virar para a esquerda.
    /// 
    /// E retornado um inteiro para simplesmente inverter caso seja a esquerda.
    /// </summary>
    /// <param name="v1">vetor 1</param>
    /// <param name="v2">vetor 2</param>
    /// <returns>1 caso esteja a direita e -1 caso esteja a esquerda.</returns>
    private int IsAtRight(Vector3 v1, Vector3 v2)
    {
        if (v1.sqrMagnitude > 1)
            v1 = v1.normalized;

        if (v2.sqrMagnitude > 1)
            v2 = v2.normalized;

        if (v1.z * v2.x > v1.x * v2.z)
            return 1;
        else
            return -1;
    }

    /// <summary>
    /// Rota a roda para atingir o alvo.
    /// </summary>
    private void LookAtTarget()
    {
        if (targetIndex >= targetList.Count)
            return;

        bool isFirst = (targetIndex == 0);
        bool keepDistanceFromTargetUpdatedIndex = true;

        if (keepDistanceFromTarget != null)
        {
            keepDistanceFromTargetUpdatedIndex = (targetVehicleBehaviour.targetIndex > 0);
        }

        if (isFirst && !keepDistanceFromTargetUpdatedIndex)
        {
            m_vehicleEngine.RotateToTarget(targetVehicleBehaviour.m_Back.position + DirCorrection());
        } else
        {
            if (dangerousObstacle == null)
                m_vehicleEngine.RotateToTarget(targetList[targetIndex].position);
            else
                m_vehicleEngine.RotateToTarget(dangerousObstacle.info.position + DirCorrection());

            // draw the movement direction vector
            Debug.DrawRay(transform.position, (targetList[targetIndex].position + DirCorrection() - transform.position).normalized * 100f, Color.blue);
        }
    }

    /// <summary>
    /// Testa se é para o veículo que eu tenho que seguir esta a minha frente ou não. Usado para testar se tu deve esperar o veaculo ou não.
    /// </summary>
    /// <returns>Retorna TRUE se eu estou na frente e FALSE se estou atras.</returns>
    private bool myKeepDistanceFromTargetIsAhead()
    {
        if (keepDistanceFromTarget != null && targetList[targetIndex] != targetVehicleBehaviour.targetList[targetIndex])
        {
            keepDistanceFromTarget = null;
        }

        if (keepDistanceFromTarget != null)
        {
            if (targetIndex -1 == targetVehicleBehaviour.targetIndex)
            {
                if (ItineraryProgress >= targetVehicleBehaviour.ItineraryProgress)
                {
                    return false;
                }
            } else if (targetIndex > targetVehicleBehaviour.targetIndex)
            {
                return false;
            }
        }
        
        return true;
    }

    /// <summary>
    /// Calcula a distancia entre eu e other;
    /// </summary>
    /// <param name="other">O outro transform.</param>
    /// <returns>A distancia entre eu e other.</returns>
    public float DistanceFromTarget(Transform other)
    {
		if (m_Front == null || other == null)
			return 0.0f;

        Vector3 m_position = m_Front.position;
        Vector3 targetPosition = other.position;

        m_position.y = targetPosition.y = 0f;
        
        return Vector3.Distance(m_position, targetPosition);
    }

    /// <summary>
    /// Seta uma lista de alvos para o veículo seguir, sem manter uma distancia minima.
    /// </summary>
    /// <param name="targetList">Lista de alvos</param>
    public void setItinerary(List<Transform> targetList)
    {
        GameObject currentPos = new GameObject("initialPosition");
        currentPos.transform.position = m_Front.position;

        this.targetList = targetList;
        this.targetList.Insert(0, currentPos.transform);

        ItineraryLength = DistanceFromTarget(targetList[1]);
        keepDistanceFromTarget = null;
    }

    /// <summary>
    /// Set the list of targets wich the vehicle will go throw and the target wich it will keep distance enought not to collide.
    /// </summary>
    /// <param name="targetList">List of targets</param>
    /// <param name="keepDistanceFromTarget">Target to keep distance from</param>
    public void setItinerary(List<Transform> targetList, Transform keepDistanceFromTarget)
    {
        GameObject currentPos = new GameObject("initialPosition");
        currentPos.transform.position = m_Front.position;

        this.targetList = targetList;
        this.targetList.Insert(0, currentPos.transform);

        ItineraryLength = DistanceFromTarget(targetList[1]);
        this.keepDistanceFromTarget = keepDistanceFromTarget;
	}

    /// <summary>
    /// Set the list of targets getting from the childs of parentTargetList wich the vehicle will go throw and the target wich it will keep distance enought not to collide.
    /// </summary>
    /// <param name="parentTargetList">Parent of the target list</param>
    /// <param name="keepDistanceFromTarget">Target to keep distance from</param>
    public void setItinerary(Transform parentTargetList, Transform keepDistanceFromTarget)
    {
        targetList.Clear();

        GameObject currentPos = new GameObject("initialPosition");
        currentPos.transform.position = m_Front.position;
        
        targetList.Add(currentPos.transform);

        foreach (Transform child in parentTargetList)
        {
            if (child == parentTargetList)
                continue;

            targetList.Add(child);
        }

        this.keepDistanceFromTarget = keepDistanceFromTarget;
        ItineraryLength = DistanceFromTarget(targetList[1]);
    }

    /// <summary>
    /// Calcula o waypoint inicial do veiculo.
    /// 
    /// E necessario pois o itinerario novo cria pontos adicinais para a posicao inicial da viatura.
    /// Entao, caso bote 3 viaturas, e criado 3 pontos adicionais e enviado para as 3, assim a viatura 1 comeca no
    /// 3 ponto e a viatura 2 no segundo e a viatura 3 no primeiro, por exemplo.
    /// </summary>
    /// <returns>O indice do waypoint inicial do veiculo.</returns>
    private int GetClosestWayPoint()
    {
        int waypoint = 0;
        
        for (int i = 0; i < targetList.Count; i++)
        {
            if (DistanceFromTarget(targetList[i]) < 1)
            {
                waypoint = i;
            }
        }

        return waypoint +1;
    }

	public void AddOnArriveCallback(Action<FinishStatus> cb)
	{
		if(m_OnArriveCallbackDelegate == null)
		{
			m_OnArriveCallbackDelegate = new Action<FinishStatus>(cb);
			return;
		}

		m_OnArriveCallbackDelegate += cb;
    }

    /// <summary>
    /// Desenha os sensores
    /// </summary>
    private void OnDrawGizmos()
    {
        if (m_Front != null)
        {
            Vector3 myForward = m_Front.forward;
            myForward.y = 0f;
            myForward = myForward.normalized;

            Vector3 myRight = m_Front.right;
            myRight.y = 0f;
            myRight = myRight.normalized;

            #region dangerous obstacle sensor
            {
                Vector3 rectangleCenter = m_Front.position + myForward * seeAhead / 2f;
                Vector3 rectangleExtents = new Vector3(dangerousObstacleCubeWidthDetection / 2f, 3f, seeAhead / 2f);

                // dangerous obstacle rectangle
                Vector3 pos1, pos2, pos3, pos4;
                pos1 = rectangleCenter + myForward * rectangleExtents.z - myRight * rectangleExtents.x;
                pos2 = rectangleCenter + myForward * rectangleExtents.z + myRight * rectangleExtents.x;
                pos3 = rectangleCenter - myForward * rectangleExtents.z + myRight * rectangleExtents.x;
                pos4 = rectangleCenter - myForward * rectangleExtents.z - myRight * rectangleExtents.x;

                Gizmos.color = Color.blue;

                Gizmos.DrawLine(pos1, pos2);
                Gizmos.DrawLine(pos2, pos3);
                Gizmos.DrawLine(pos3, pos4);
                Gizmos.DrawLine(pos4, pos1);
            }
            #endregion 

            #region Sensor
            {
                Vector3 rectangleCenter = m_Front.position + myForward * sensorHeight / 2f;
                Vector3 rectangleExtents = new Vector3(sensorWidth / 2f, 3f, sensorHeight / 2f);

                // dangerous obstacle rectangle
                Vector3 pos1, pos2, pos3, pos4;
                pos1 = rectangleCenter + myForward * rectangleExtents.z - myRight * rectangleExtents.x;
                pos2 = rectangleCenter + myForward * rectangleExtents.z + myRight * rectangleExtents.x;
                pos3 = rectangleCenter - myForward * rectangleExtents.z + myRight * rectangleExtents.x;
                pos4 = rectangleCenter - myForward * rectangleExtents.z - myRight * rectangleExtents.x;

                Gizmos.color = Color.green;

                Gizmos.DrawLine(pos1, pos2);
                Gizmos.DrawLine(pos2, pos3);
                Gizmos.DrawLine(pos3, pos4);
                Gizmos.DrawLine(pos4, pos1);
            }
            #endregion
        }
    }

    /// <summary>
    /// Desenha a lista de obstáculos
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        for (int i = 0; i < targetList.Count; i++)
		{
			if (targetList[i] == null)
				continue;

			Gizmos.color = Color.Lerp(Color.red, Color.green, /*targetListReverseOrder ? (targetList.Count - 1 - i) / (float)targetList.Count : */i / (float)targetList.Count);
			Gizmos.DrawSphere(targetList[i].position, 10);

			if (i < targetList.Count - 1 && targetList[i + 1] != null)
				Gizmos.DrawLine(targetList[i].position, targetList[i + 1].position);
		}
	}
    
    /// <summary>
    /// Sensor do Steering Behaviour para analizar objetos próximos.
    /// </summary>
    private void Sensor()
    {
        Vector3 myForward = m_Front.forward;
        myForward.y = 0f;
        myForward = myForward.normalized;

        Vector3 rectangleCenter = m_Front.position + myForward * sensorHeight / 2f;
        Vector3 rectangleExtents = new Vector3(sensorWidth / 2f, 5f, sensorHeight / 2f);

        //allTrees = treeHash.GetAllTreesInNode(transform.position);

        TreeSensor(rectangleCenter, rectangleExtents, treeColliderRadius);

        ComponentSensor(rectangleCenter, rectangleExtents);
    }

    private void ComponentSensor(Vector3 rectangleCenter, Vector3 rectangleExtents)
    {
        Collider[] objects = Physics.OverlapBox(rectangleCenter, rectangleExtents);

        for(int i = 0; i < objects.Length; i++)
        {
            if (objects[i].attachedRigidbody != null)
            {
                if (objects[i].attachedRigidbody.gameObject == gameObject)
                    continue;

                ObstacleCircleCollider info = objects[i].attachedRigidbody.gameObject.GetComponent<ObstacleCircleCollider>();
                if (info != null && keepDistanceFromTarget != info.transform && GetObjectInObstacles(info) == null)
                {
                    obstacles.Add(new VehicleBehaviourObstacleInfo(info, false));
                }
            }
        }

        for (int i = 0; i < obstacles.Count; i++)
        {
            if (obstacles[i].isTree)
                continue;

            bool found = false;
            for (int j = 0; j < objects.Length; j++)
            {
                if (objects[j].attachedRigidbody != null)
                {
                    ObstacleCircleCollider info = objects[j].attachedRigidbody.gameObject.GetComponent<ObstacleCircleCollider>();

                    if (info != null && GetObjectInObstacles(info) == obstacles[i])
                    {
                        found = true;
                        break;
                    }
                }
            }

            if (!found)
                obstacles.Remove(obstacles[i]);
        }
    }

    private void TreeSensor(Vector3 rectangleCenter, Vector3 rectangleExtents, float treeRadius)
    {
        for (int i = 0; i < allTrees.Length; i++)
        {
            if (IntersectsWithRectangle(rectangleCenter, rectangleExtents, allTrees[i], treeRadius))
            {
                VehicleBehaviourObstacleInfo treeInfo = GetTreeInObstacles(allTrees[i]);
                if (treeInfo == null)
                {
                    if (parentOfTrees == null)
                    {
                        parentOfTrees = new GameObject("parentOfTrees").transform;
                    }
                    // 12 23 27 24
                    Transform t = new GameObject("TreeHelper").transform;
                    t.parent = parentOfTrees;
                    t.position = allTrees[i];
                    ObstacleCircleCollider info = t.gameObject.AddComponent<ObstacleCircleCollider>();

                    info.radius = treeRadius;

                    obstacles.Add(new VehicleBehaviourObstacleInfo(info, true));
                }
            } else
            {
                VehicleBehaviourObstacleInfo treeInfo = GetTreeInObstacles(allTrees[i]);
                if (treeInfo != null && treeInfo.isTree)
                {
                    obstacles.Remove(treeInfo);

                    if (treeInfo.isTree)
                    {
                        DestroyImmediate(treeInfo.info.gameObject);

                        if (dangerousObstacle == treeInfo)
                            dangerousObstacle = null;
                    }
                }
            }
        }
    }

    private VehicleBehaviourObstacleInfo GetTreeInObstacles(Vector3 tree)
    {
        for(int i = 0; i < obstacles.Count; i++)
        {
            if (!obstacles[i].isTree)
                continue;

            if ((obstacles[i].info.position - tree).sqrMagnitude < 0.25f)
                return obstacles[i];
        }

        return null;
    }

    private VehicleBehaviourObstacleInfo GetObjectInObstacles(ObstacleCircleCollider info)
    {
        for (int i = 0; i < obstacles.Count; i++)
        {
            if (obstacles[i].isTree)
                continue;

            if (obstacles[i].info == info)
                return obstacles[i];
        }

        return null;
    }
}