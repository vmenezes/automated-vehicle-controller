﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleRectangleCollider : ObstacleCollider {
    
    [SerializeField] private float _width = 1f;
    [SerializeField] private float _height = 1f;

    // topLeft, topRight, bottomRight, bottomLeft;
    [HideInInspector] public Vector3[] corners = new Vector3[4];

    public bool debugCollisionCounter = false;

    private void Awake()
    {
        
    }

    private void Start()
    {
        if (corners.Length != 4)
            corners = new Vector3[4];

        if (debugCollisionCounter)
            CollisionManager.instance.RegisterCollider(this);
    }

    private void Update()
    {
        if (corners != null)
        {
            corners[0] = topLeftCorner;
            corners[1] = topRightCorner;
            corners[2] = bottomRightCorner;
            corners[3] = bottomLeftCorner;
        }
    }

    private void OnDestroy()
    {
        if (debugCollisionCounter)
            CollisionManager.instance.UnregisterCollider(this);
    }

    public float width
    {
        set { _width = value; }
        get { return _width; }
    }
    
    public float height
    {
        set { _height = value; }
        get { return _height; }
    }

    public Vector3 topLeftCorner
    {
        get { return position + transform.forward * height / 2f - transform.right * width / 2f; }
    }

    public Vector3 topRightCorner
    {
        get { return position + transform.forward * height / 2f + transform.right * width / 2f; }
    }

    public Vector3 bottomRightCorner
    {
        get { return position - transform.forward * height / 2f + transform.right * width / 2f; }
    }

    public Vector3 bottomLeftCorner
    {
        get { return position - transform.forward * height / 2f - transform.right * width / 2f; }
    }

    private void OnDrawGizmosSelected()
    {
        
        Vector3 rectangleCenter = position;
        Vector3 rectangleExtents = new Vector3(width / 2f, 3f, height / 2f);

        // dangerous obstacle rectangle
        Vector3 pos1, pos2, pos3, pos4;
        pos1 = rectangleCenter + transform.forward * rectangleExtents.z - transform.right * rectangleExtents.x;
        pos2 = rectangleCenter + transform.forward * rectangleExtents.z + transform.right * rectangleExtents.x;
        pos3 = rectangleCenter - transform.forward * rectangleExtents.z + transform.right * rectangleExtents.x;
        pos4 = rectangleCenter - transform.forward * rectangleExtents.z - transform.right * rectangleExtents.x;

        if (debugCollisionCounter)
        {
            Gizmos.color = Color.magenta;
        } else
        {
            Gizmos.color = Color.green;
        }

        Gizmos.DrawLine(pos1, pos2);
        Gizmos.DrawLine(pos2, pos3);
        Gizmos.DrawLine(pos3, pos4);
        Gizmos.DrawLine(pos4, pos1);
    }
}
