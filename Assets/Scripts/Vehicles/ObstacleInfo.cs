﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleInfo : MonoBehaviour {

    [SerializeField] private Vector3 _position = Vector3.zero;
	[SerializeField] private float _radius = 1f;

    public float radius
    {
        set { _radius = value; }
        get { return _radius; }
    }

    public Vector3 localPosition
    {
        get { return _position; }
    }

    public Vector3 position
    {
        get { return transform.position + transform.rotation * _position; }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawWireSphere(position, radius);
    }
}
