﻿using UnityEngine;
namespace ASTROS.Behaviour.Vehicles.Controllers
{
    /// <summary>
    /// Motor do veiculo. Essa classe e responsavel pela implementacao das funcoes da fisica do veiculo como acelerar, freiar,
    /// rotar as rodas para um algo. Toda outra classe que necessitam de tais funcoes devem utiliza-las daqui.
    /// </summary>
    public class VehicleEngine : MonoBehaviour
    {
        /// <summary>
        /// Colisores da roda, arrastados do modelo do veiculo dentro da unity
        /// </summary>
        public WheelCollider[] m_wheelCollider;
        /// <summary>
        /// Transform da roda, arrastados do modelo do veiculo dentro da unity
        /// </summary>
        public Transform[] m_wheelTransform;

        /// <summary>
        /// Velocidade instantanea do veiculo
        /// </summary>
        [SerializeField]
        private float m_velocity; // My velocity
        public float velocity // Get/Set velocity
        {
            get { return m_velocity; }
            private set { m_velocity = value; }
        }
        private Rigidbody m_rigidBody;

        /// <summary>
        /// Torque maximo aplicado na roda do veiculo.
        /// </summary>
        [SerializeField]
        private float maxTorque = 500.0f;
        /// <summary>
        /// Torque aplicado ao se freiar bruscamente.
        /// </summary>
        [SerializeField]
        private float brakeTorque = 2000.0f;
        /// <summary>
        /// Forca para equilibrar o veiculo. Barra anti-derrapante
        /// </summary>
        [SerializeField]
        private float antiRoll = 4375.0f;

        /// <summary>
        /// Centro de massa do veiculo. Geralmente os veiculos tem um centro de massa proximo ao chao, a Unity calcula o centro baseado
        /// na media das posicoes dos colisores e normalmente deve ser modificado para reproduzir mais fielmente a realidade.
        /// </summary>
        [SerializeField]
        private Vector3 centerOfMass = Vector3.zero; // rigidBody's center of mass.

        /// <summary>
        /// Rotacao vertical maxima aplicada as rodas do veiculo
        /// </summary>
        public const float carMaxWheelTurn = 33f;

        void Start()
        {
            m_rigidBody = GetComponent<Rigidbody>();
            m_rigidBody.centerOfMass = centerOfMass;
        }

        void Update()
        {
            RotateWheels();
        }

        void FixedUpdate()
        {
            velocity = m_rigidBody.velocity.magnitude * 3.6f;

            StabilizerBar();
        }

        /// <summary>
        /// Rota as rodas no eixo vertical em direcao ao dado alvo(target).
        /// </summary>
        /// <param name="target">Alvo no qual as rodas irao rotar para que o veiculo siga em direcao.</param>
        public void RotateToTarget(Vector3 target)
        {
            Vector3 myForward = Quaternion.Euler(new Vector3(0, transform.eulerAngles.y, transform.eulerAngles.z)) * Vector3.forward;
            Vector3 myTargetPos = target;
            myTargetPos.y = transform.position.y;

            float angleBetweenForwards = Vector3.Angle(myForward, (myTargetPos - transform.position).normalized);
            short direction = (short)((transform.InverseTransformPoint(myTargetPos).x > 0) ? 1 : -1);

            m_wheelCollider[0].steerAngle = Mathf.SmoothStep(m_wheelCollider[0].steerAngle, Mathf.Min(angleBetweenForwards, 30f) * direction, Time.deltaTime * 10f);
            m_wheelCollider[1].steerAngle = m_wheelCollider[0].steerAngle;
        }

        /// <summary>
        /// Rota as rodas no eixo horizontal para a reproducao da roda girar ao andar.
        /// </summary>
        void RotateWheels()
        {
            for (int i = 0; i < m_wheelCollider.Length; i++)
            {
                if (!m_wheelCollider[i].enabled)
                    continue;

                m_wheelTransform[i].localEulerAngles = new Vector3(m_wheelTransform[i].localEulerAngles.x, m_wheelCollider[i].steerAngle - m_wheelTransform[i].localEulerAngles.z, m_wheelTransform[i].localEulerAngles.z);

                m_wheelTransform[i].Rotate(Vector3.right, m_wheelCollider[i].rpm * 6f * Time.deltaTime);
            }
        }

        /// <summary>
        /// Freia com forca brakeTorque. E uma freiada brusca.
        /// </summary>
        public void Brake()
        {
            if (Mathf.Approximately(velocity, 0.1f) || velocity < 0.1f)
                return;

            foreach (WheelCollider c in m_wheelCollider)
            {
                c.brakeTorque = brakeTorque;
                c.motorTorque = 0f;
            }
        }

        /// <summary>
        /// Freio motor. Nao e uma freiada brusco.
        /// </summary>
        public void EngineBrake()
        {
            if (Mathf.Approximately(velocity, 0.1f) || velocity < 0.1f)
                return;

            foreach (WheelCollider c in m_wheelCollider)
            {
                c.brakeTorque += Time.deltaTime * 10f;
                c.motorTorque = 0f;
            }
        }

        /// <summary>
        /// Acelera utilizando o maxTorque como forca aplicada nas rodas para a movimentacao do veiculo.
        /// </summary>
        public void Accelerate(bool forward)
        {
            foreach (WheelCollider c in m_wheelCollider)
            {
                c.brakeTorque = 0f;

                if (Mathf.Abs(c.motorTorque) < maxTorque / 3f)
                {
                    c.motorTorque = maxTorque / 3f * ((forward) ? 1 : -1);
                }

                c.motorTorque += maxTorque / 10f * Time.deltaTime * ((forward) ? 1 : -1);

                c.motorTorque = Mathf.Clamp(c.motorTorque, -maxTorque, maxTorque);
            }
        }

        /// <summary>
        /// Barra estabilizadora(Anti-Roll Bars) para deixar o veiculo mais estavel e nao capotar.
        /// http://www.edy.es/dev/2011/10/the-stabilizer-bars-creating-physically-realistic-stable-vehicles/
        /// </summary>
        void StabilizerBar()
        {
            for (int i = 0; i < m_wheelCollider.Length; i += 2)
            {
                var WheelL = m_wheelCollider[i];
                var WheelR = m_wheelCollider[i + 1];
                WheelHit hit;
                bool groundedL = WheelL.GetGroundHit(out hit);
                float travelL;
                if (groundedL)
                    travelL = (-WheelL.transform.InverseTransformPoint(hit.point).y - WheelL.radius)
                                / WheelL.suspensionDistance;
                else
                    travelL = 1.0f;

                bool groundedR = WheelR.GetGroundHit(out hit);
                float travelR;
                if (groundedR)
                    travelR = (-WheelR.transform.InverseTransformPoint(hit.point).y - WheelR.radius)
                                / WheelR.suspensionDistance;
                else
                    travelR = 1.0f;

                float antiRollForce = 0.0f;

                if (Mathf.Abs(travelL - travelR) > 0.25f)
                    antiRollForce = (travelL - travelR) * antiRoll;

                if (groundedL)
                    m_rigidBody.AddForceAtPosition(transform.up * -antiRollForce, WheelL.transform.position);
                if (groundedR)
                    m_rigidBody.AddForceAtPosition(transform.up * antiRollForce, WheelR.transform.position);
            }
        }
    }
}