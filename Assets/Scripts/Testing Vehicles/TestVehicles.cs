﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ASTROS.Behaviour.Vehicles;

public class TestVehicles : MonoBehaviour {

	[SerializeField] GameObject vehicles;

    Transform fatherOfTemps;

    private void Start()
    {
        if (vehicles == null)
            return;

        List<VehicleBehaviour> controllers = new List<VehicleBehaviour>();

        for (int i = 0; i < vehicles.transform.childCount; i++)
        {
            VehicleBehaviour b = vehicles.transform.GetChild(i).GetComponent<VehicleBehaviour>();

            if (b != null)
                controllers.Add(b);
        }

        fatherOfTemps = new GameObject("Father Of Temps").transform;

        for (int i = 0; i < controllers.Count; i++)
        {
            List<Transform> itinerary = new List<Transform>();

            GameObject finalPoint = new GameObject("finalPoint");
            finalPoint.transform.parent = fatherOfTemps;
            finalPoint.transform.position = controllers[(i + controllers.Count / 2) % controllers.Count].transform.position;
            itinerary.Add(finalPoint.transform);

            controllers[i].SetItinerary(itinerary, true);
        }

        for (int i = 0; i < controllers.Count; i++)
        {
            if (!controllers[i].gameObject.activeSelf)
                continue;

            controllers[i].isDriving = true;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.KeypadPlus))
        {
            Time.timeScale += 1;

            if (Time.timeScale > 10)
                Time.timeScale = 10;
        }

        if (Input.GetKeyDown(KeyCode.KeypadMinus))
        {
            Time.timeScale -= 1;

            if (Time.timeScale < 1)
                Time.timeScale = 1;
        }
    }
}
